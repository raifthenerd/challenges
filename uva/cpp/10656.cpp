#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    size_t N; cin >> N; if (N == 0) break;
    vector<size_t> seq;
    while (N--) {
      size_t tmp; cin >> tmp;
      if (tmp != 0) seq.push_back(tmp);
    }
    if (seq.empty()) cout << "0\n";
    else {
      auto first = true;
      for (auto c : seq) {
        if (first) first = false;
        else cout << ' ';
        cout << c;
      }
      cout << '\n';
    }
  }
}
