#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  bool first = true;
  while (true) {
    int k; cin >> k; if (k == 0) break;
    if (!first) cout << '\n'; else first = false;
    vector<int> nums; while (k--) { int n; cin >> n; nums.push_back(n); }
    sort(nums.begin(), nums.end());
    for (size_t a = 0; a < nums.size(); ++a)
      for (size_t b = a+1; b < nums.size(); ++b)
        for (size_t c = b+1; c < nums.size(); ++c)
          for (size_t d = c+1; d < nums.size(); ++d)
            for (size_t e = d+1; e < nums.size(); ++e)
              for (size_t f = e+1; f < nums.size(); ++f)
                cout << nums[a] << ' '
                     << nums[b] << ' '
                     << nums[c] << ' '
                     << nums[d] << ' '
                     << nums[e] << ' '
                     << nums[f] << '\n';
  }
}
