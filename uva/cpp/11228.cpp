#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int n; vii cities; int graph[1000][1000]; bool taken[1000];
priority_queue<ii> pq;

int distSq(ii a, ii b) {
  auto dx = a.first - b.first, dy = a.second - b.second;
  return dx * dx + dy * dy;
}
void process(int v) {
  taken[v] = true;
  for (auto i = 0; i < n; ++i)
    if (!taken[i]) pq.push({-graph[i][v], -i});
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    int r; cin >> n >> r;
    cities.clear();
    for (auto i = 0; i < n; ++i) {
      int x, y; cin >> x >> y; cities.push_back({x, y});
    }
    for (auto i = 0; i < n; ++i) {
      taken[i] = false; graph[i][i] = 0;
      for (auto j = i + 1; j < n; ++j)
        graph[i][j] = graph[j][i] = distSq(cities[i], cities[j]);
    }
    process(0);
    int states = 1; double road = 0.0, railroad = 0.0;
    while (!pq.empty()) {
      auto tmp = pq.top(); pq.pop();
      auto u = -tmp.second, w = -tmp.first;
      if (!taken[u]) {
        if (w > r*r)
          ++states, railroad += sqrt(w);
        else
          road += sqrt(w);
        process(u);
      }
    }
    cout << "Case #" << t << ": "
         << states << ' '
         << lround(road) << ' '
         << lround(railroad) << '\n';
  }
}
