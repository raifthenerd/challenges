#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 1;
int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  cout << fixed << setprecision(3);
  int T = 0;
  while (++T) {
    int graph[100][100];
    REP(i, 0, 100) {
      graph[i][i] = 0;
      REP(j, i + 1, 100) graph[i][j] = graph[j][i] = oo;
    }
    set<int> Vs;
    while (true) {
      int a, b; cin >> a >> b; if (a == 0 && b == 0) break; --a; --b;
      graph[a][b] = 1; Vs.insert(a); Vs.insert(b);
    }
    if (SZ(Vs) == 0) break;
    TRAV(k, Vs) TRAV(i, Vs) TRAV(j, Vs)
      graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);
    auto n = 0, result = 0;
    TRAV(i, Vs) TRAV(j, Vs) if (i != j) ++n, result += graph[i][j];
    cout << "Case " << T << ": average length between pages = "
         << static_cast<double>(result) / n << " clicks\n";
  }
}
