#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int c; cin >> c; while (c--) {
    int n, m; cin >> n >> m;
    vector<vii> outgoing; outgoing.assign(n, {});
    while (m--) {
      int x, y, t; cin >> x >> y >> t; outgoing[x].push_back({y, t});
    }
    vi dist(n, oo); dist[0] = 0;
    REP(i, 0, n-1) REP(u, 0, n) TRAV(v, outgoing[u])
      dist[v.first] = min(dist[v.first], dist[u] + v.second);
    bool negcycle = false;
    REP(u, 0, n) TRAV(v, outgoing[u])
      if (dist[v.first] > dist[u] + v.second)
        negcycle = true;
    if (!negcycle) cout << "not "; cout << "possible\n";
  }
}
