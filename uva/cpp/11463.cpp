#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 2;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T; cin >> T;
  REP(t, 1, T+1) {
    int N, R; cin >> N >> R;
    int graph[N][N];
    REP(i, 0, N) {
      graph[i][i] = 0;
      REP(j, i+1, N) graph[i][j] = graph[j][i] = oo;
    }
    while (R--) {
      int u, v; cin >> u >> v; graph[u][v] = graph[v][u] = 1;
    }
    REP(k, 0, N) REP(i, 0, N) REP(j, 0, N)
      graph[i][j] = min(graph[i][j], graph[i][k] + graph[k][j]);
    int s, d; cin >> s >> d;
    auto result = 0;
    REP(i, 0, N) result = max(result, graph[s][i] + graph[i][d]);
    cout << "Case " << t << ": " << result << '\n';
  }
}
