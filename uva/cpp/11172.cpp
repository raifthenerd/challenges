#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int t, a, b;
  cin >> t;
  for (auto i = 0; i < t; ++i) {
    cin >> a >> b;
    if (a > b) {
      cout << ">\n";
    } else if (a < b) {
      cout << "<\n";
    } else {
      cout << "=\n";
    }
  }
}
