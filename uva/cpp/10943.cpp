#include <bits/stdc++.h>

using namespace std;

int P[200][100];
int Cmod(int n, int k) {
  if (k == 0 || k == n) return 1;
  if (P[n][k] == -1) {
    P[n][k] = (Cmod(n-1, k-1) + Cmod(n-1, k)) % 1000000;
  }
  return P[n][k];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  for (auto i = 0; i < 200; ++i)
    for (auto j = 0; j < 100; ++j)
      P[i][j] = -1;
  while (true) {
    int N, K; cin >> N >> K; if (N == 0 && K == 0) break;
    cout << Cmod(N + K - 1, K - 1) << '\n';
  }
}
