#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n, m, total;
  cin >> total;
  for (auto i = 0; i < total; ++i) {
    cin >> n >> m;
    cout << (m / 3) * (n / 3) << "\n";
  }
}
