#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, tmp, credit = 0;
  string s;
  cin >> N;
  for (auto i = 0; i < N; ++i) {
    cin >> s;
    if (s == "report") cout << credit << "\n";
    else if (s == "donate") {
      cin >> tmp;
      credit += tmp;
    }
  }
}
