#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int i = 0;
  while (++i) {
    int n, m, cap; cin >> n >> m >> cap;
    if (n == 0 && m == 0 && cap == 0) break;
    cout << "Sequence " << i << '\n';
    bool blown = false; int pow = 0, maxpow = 0;
    vector<int> c(n), swtch(n, -1);
    for (auto j = 0; j < n; ++j) cin >> c[j];
    while (m--) {
      cin >> n; --n;
      swtch[n] *= -1;
      pow += c[n] * swtch[n];
      if (pow > cap)
        blown = true;
      else
        maxpow = max(pow, maxpow);
    }
    cout << "Fuse was " << (blown ? "" : "not ") << "blown.\n";
    if (!blown)
      cout << "Maximal power consumption was " << maxpow << " amperes.\n";
    cout << '\n';
  }
}
