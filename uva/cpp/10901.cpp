#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t c; cin >> c;
  while (c--) {
    size_t n, t, m; cin >> n >> t >> m;
    queue<pair<size_t, size_t>> left, right;
    for (size_t i = 0; i< m; ++i) {
      size_t arrived; string dir; cin >> arrived >> dir;
      auto tmp = make_pair(i, arrived);
      if (dir == "left") left.push(tmp);
      else right.push(tmp);
    }
    size_t now = 0;
    auto *curr = &left, *other = &right;
    vector<size_t> result(m);
    while (true) {
      size_t ferry = 0;
      pair<size_t, size_t> tmp;
      if (curr->empty()) {
        if (other->empty()) break;
        else tmp = other->front();
      } else {
        if (other->empty()) tmp = curr->front();
        else tmp = min(curr->front(), other->front());
      }
      now = max(now, tmp.second);
      while (!curr->empty() && curr->front().second <= now && ferry++ < n) {
        result[curr->front().first] = now + t; curr->pop();
      }
      now += t;
      swap(curr, other);
    }
    for (auto&& res : result) cout << res << '\n';
    if (c) cout << '\n';
  }
}
