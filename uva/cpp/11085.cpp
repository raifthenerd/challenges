#include <bits/stdc++.h>

using namespace std;

int queens[8], q[8], result;

bool check(int idx) {
  for (auto i = 1; i <= idx; ++i) {
    for (auto j = 0; j < i; ++j) {
      if (q[i] == q[j] || abs(q[i] - q[j]) == i-j) return false;
    }
  }
  return true;
}

void bt(int idx) {
  if (result == 0) return;
  if (idx == 8) {
    int tmp = 0;
    for (auto i = 0; i < 8; ++i) if (queens[i] != q[i]) ++tmp;
    result = min(result, tmp);
  } else {
    for (auto i = 1; i <= 8; ++i) {
      if (check(idx)) bt(idx+1);
      q[idx] = (queens[idx]+i) % 8;
    }
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t t = 0;
  while (cin >> queens[0]) {
    cout << "Case " << ++t << ": ";
    for (auto i = 1; i < 8; ++i) cin >> queens[i];
    for (auto i = 0; i < 8; ++i) q[i] = --queens[i];
    result = 7; bt(0);
    cout << result << '\n';
  }
}
