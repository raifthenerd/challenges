#include <bits/stdc++.h>

using namespace std;

int total; bool found;
vector<int> keys;
map<int, int> xs, vals;

void bt(size_t idx) {
  if (idx == keys.size()) return;
  for (auto i = xs[keys[idx]]; i >= 0; --i) {
    vals[keys[idx]] = i;
    auto res = 0;
    for (auto&& c : vals) res += c.first * c.second;
    if (res == total) {
      found = true; bool prefix = false;
      for (auto key : keys) {
        for (auto i = 0; i < vals[key]; ++i) {
          if (prefix) cout << '+';
          else prefix = true;
          cout << key;
        }
      }
      cout << '\n';
    } else if (res < total) {
      bt(idx + 1);
    }
  }
}
int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int n; cin >> total >> n; if (total == 0 && n == 0) break;
    cout << "Sums of " << total << ":\n";
    xs.clear(); while (n--) {
      int x; cin >> x; ++xs[x];
    }
    keys.clear(); vals.clear(); for (auto x : xs) {
      keys.insert(keys.begin(), x.first); vals[x.first] = 0;
    }
    found = false; bt(0);
    if (!found) cout << "NONE\n";
  }
}
