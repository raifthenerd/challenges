#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, W;
  cin >> N;
  for (auto i = 0; i < N; ++i) {
    if (i > 0) cout << "\n";
    cin >> W;
    int y1, y2;
    cin >> y1 >> y2;
    int gap = y1 - y2;
    bool closed = true;
    for (auto j = 1; j < W; ++j) {
      cin >> y1 >> y2;
      if (gap != y1 - y2) closed = false;
    }
    cout << (closed ? "yes\n" : "no\n");
  }
}
