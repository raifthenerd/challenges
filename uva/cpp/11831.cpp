#include <bits/stdc++.h>

using namespace std;

const map<char, int> direction {{'L', 0},
                                {'S', 1},
                                {'O', 2},
                                {'N', 3}};
struct robot {
  int x, y, dir;
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int N, M, S; cin >> N >> M >> S; if (N == 0 && M == 0 && S == 0) break;
    char board[N][M]; robot r;
    for (auto y = 0; y < N; ++y) {
      for (auto x = 0; x < M; ++x) {
        cin >> board[y][x];
        switch (board[y][x]) {
          case 'N': case 'S': case 'L': case 'O':
            r.y = y; r.x = x; r.dir = direction.at(board[y][x]);
            break;
        }
      }
    }
    int result = 0;
    while (S--) {
      char cmd; cin >> cmd;
      switch(cmd) {
        case 'D': r.dir = (r.dir + 1) % 4; break;
        case 'E': r.dir = (r.dir + 3) % 4; break;
        case 'F':
          auto x = r.x, y = r.y;
          if (r.dir % 2 == 0) x += (r.dir < 2 ? 1 : -1);
          else y += (r.dir < 2 ? 1 : -1);
          if (0 <= x && x < M && 0 <= y && y < N && board[y][x] != '#') {
            if (board[y][x] == '*') {
              board[y][x] = '.'; ++result;
            }
            r.x = x; r.y = y;
          }
          break;
      }
    }
    cout << result << '\n';
  }
}
