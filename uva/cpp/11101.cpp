#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();
const vii diffs = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
int dist[2001][2001];

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    vii start; set<ii> target;
    int p; cin >> p; if (p == 0) return 0;
    while (p--) {
      int a, s; cin >> a >> s; start.push_back({a, s});
    }
    cin >> p;
    while (p--) {
      int a, s; cin >> a >> s; target.insert({a, s});
    }
    REP(a, 0, 2001) REP(s, 0, 2001) dist[a][s] = -1;
    queue<ii> q; bool found = false;
    TRAV(c, start) {
      if (target.count(c) > 0) {
        found = true; cout << "0\n"; break;
      }
      q.push(c), dist[c.first][c.second] = 0;
    }
    while (!found && !q.empty()) {
      auto tmp = q.front(); q.pop();
      auto a = tmp.first, s = tmp.second;
      TRAV(diff, diffs) {
        auto tmpa = a + diff.first, tmps = s + diff.second;
        if (tmpa >= 0 && tmpa <= 2000 && tmps >= 0 && tmps <= 2000 &&
            dist[tmpa][tmps] < 0) {
          q.push({tmpa, tmps}); dist[tmpa][tmps] = dist[a][s] + 1;
          if (target.count({tmpa, tmps}) > 0) {
            found = true; cout << dist[tmpa][tmps] << '\n'; break;
          }
        }
      }
    }
  }
}
