#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  map<char, set<size_t>> S;
  size_t idx = 0; char c; while ((c = cin.get()) != '\n') S[c].insert(idx++);
  size_t Q; cin >> Q;
  while (Q--) {
    bool matched = true;
    string query; cin >> query;
    size_t l = idx, u = 0, curr = 0;
    for (auto c : query) {
      auto it = S[c].lower_bound(curr);
      if (it == S[c].end()) {
        matched = false;
        break;
      } else {
        curr = *it;
        l = min(l, curr);
        u = max(u, curr);
        ++curr;
      }
    }
    if (matched) cout << "Matched " << l << ' ' << u << '\n';
    else cout << "Not matched\n";
  }
}
