#include <bits/stdc++.h>

using namespace std;

const int INF = numeric_limits<int>::max() >> 1;

int N, P; int D[52][51][51], adj[13][13];
pair<int, int> opera[13];
bool cached[13][8192]; int mem[13][8192];

void FloydWarshall(int M) {
  for (auto k = 0; k <= N; ++k) {
    for (auto i = 0; i < N; ++i) {
      D[k][i][i] = 0;
      for (auto j = i + 1; j < N; ++j) {
        D[k][i][j] = D[k][j][i] = INF;
      }
    }
  }
  while (M--) {
    int i, j, d, a = 0; cin >> i >> j >> d; if (cin.get() == '.') cin >> a;
    d = 100 * d + a;
    if (D[0][i][j] > d) D[0][i][j] = D[0][j][i] = d;
  }
  for (auto k = 1; k <= N; ++k) {
    for (auto i = 0; i < N; ++i) {
      for (auto j = 0; j < N; ++j) {
        D[k][i][j] = min(D[k-1][i][j], D[k-1][i][k-1] + D[k-1][k-1][j]);
      }
    }
  }
}

int solve(int curr, unsigned int mask) {
  if (mask == (1U << P) - 1) return opera[curr].second - adj[curr][0];
  if (!cached[curr][mask]) {
    mem[curr][mask] = opera[curr].second - adj[curr][0];
    for (auto nxt = 1; nxt < P; ++nxt) {
      if (curr == nxt) continue;
      if (mask & (1U << nxt)) continue;
      mem[curr][mask] = max(mem[curr][mask],
                            opera[curr].second - adj[curr][nxt] + solve(nxt, mask | (1U << nxt)));
    }
    cached[curr][mask] = true;
  }
  return mem[curr][mask];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << fixed << setprecision(2);
  int T; cin >> T;
  while (T--) {
    int M; cin >> N >> M; ++N;
    FloydWarshall(M);
    cin >> P; map<int, int> tmp;
    while (P--) {
      int i, d, a = 0; cin >> i >> d; if (cin.get() == '.') cin >> a;
      tmp[i] += 100 * d + a;
    }
    P = 1; opera[0] = make_pair(0, 0); for (auto c : tmp) opera[P++] = c;
    for (auto i = 0; i < P; ++i)
      for (auto j = 0; j < P; ++j)
        adj[i][j] = D[N][opera[i].first][opera[j].first];
    for (auto i = 0; i < 13; ++i)
      for (auto j = 0; j < 8192; ++j)
        cached[i][j] = false;
    auto result = solve(0, 1);
    if (result > 0)
      cout << "Daniel can save $"
           << result/100 << "." << setfill('0') << setw(2) << result%100 << '\n';
    else cout << "Don't leave the house\n";
  }
}
