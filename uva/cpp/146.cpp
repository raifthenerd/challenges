#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    string s; cin >> s;
    if (s == "#") break;
    cout << (next_permutation(s.begin(), s.end()) ? s : "No Successor") << '\n';
  }
}
