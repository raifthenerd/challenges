#include <bits/stdc++.h>

using namespace std;

int row, col;
string imgD;
vector<vector<bool>> imgB;

bool allequal(int r, int c, int y, int x) {
  auto base = imgB[y][x];
  for (auto i = 0; i < r; ++i) {
    for (auto j = 0; j < c; ++j) {
      if (base != imgB[y+i][x+j]) return false;
    }
  }
  return true;
}

void BtoD(int r, int c, int y, int x) {
  if (allequal(r, c, y, x)) {
    imgD += imgB[y][x] ? '1' : '0';
  } else {
    imgD += 'D';
    int top = (r + 1) / 2, left = (c + 1) / 2;
    BtoD(top, left, y, x);
    if (c - left) BtoD(top, c - left, y, x + left);
    if (r - top) {
      BtoD(r - top, left, y + top, x);
      if (c - left) BtoD(r - top, c - left, y + top, x + left);
    }
  }
}

size_t DtoB(int r, int c, int y, int x, size_t idx) {
  if (imgD[idx++] == 'D') {
    int top = (r + 1) / 2, left = (c + 1) / 2;
    idx = DtoB(top, left, y, x, idx);
    if (c - left) idx = DtoB(top, c - left, y, x + left, idx);
    if (r - top) {
      idx = DtoB(r - top, left, y + top, x, idx);
      if (c - left) idx = DtoB(r - top, c - left, y + top, x + left, idx);
    }
  } else {
    for (auto i = 0; i < r; ++i) {
      for (auto j = 0; j < c; ++j) {
        imgB[y+i][x+j] = (imgD[idx-1] == '1');
      }
    }
  }
  return idx;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  char c; cin >> c;
  while (true) {
    if (c == '#') break; cout << (c == 'B' ? 'D' : 'B');
    cin >> row >> col;
    cout.width(4); cout << right << row;
    cout.width(4); cout << right << col;
    imgD.clear(); imgB.clear(); imgB.assign(row, {});
    for (auto i = 0; i < row; ++i) imgB[i].assign(col, false);
    size_t tmp = 0;
    if (c == 'B') {
      for (auto i = 0; i < row; ++i) {
        for (auto j = 0; j < col; ++j) {
          cin >> c; imgB[i][j] = c == '1';
        }
      }
      cin >> c;
      BtoD(row, col, 0, 0);
      do {
        cout << '\n' << imgD.substr(tmp, 50);
        tmp += 50;
      } while (tmp < imgD.size());
    } else if (c == 'D') {
      while (true) {
        cin >> c;
        if (c == '#' || c == 'B' || (c == 'D' && cin.peek() == ' ')) break;
        imgD += c;
        if (cin.peek() != '\n') {
          string s; cin >> s; imgD += s;
        }
      }
      DtoB(row, col, 0, 0, 0);
      for (auto i = 0; i < row; ++i) {
        for (auto j = 0; j < col; ++j) {
          if (tmp++ % 50 == 0) cout << '\n';
          cout << (imgB[i][j] ? '1' : '0');
        }
      }
    }
    cout << '\n';
  }
}
