#include <bits/stdc++.h>

using namespace std;

int n;
map<int, vector<int>> diffs;

bool check(int k) {
  auto idx = 0;
  while (k > 0) {
    if (diffs[k].size() == 0) return true;
    auto it = lower_bound(diffs[k].begin(), diffs[k].end(), idx);
    if (it == diffs[k].end()) return true;
    if (next(it) != diffs[k].end()) return false;
    idx = *it; --k;
  }
  return idx == n;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    cin >> n; diffs.clear();
    int rprev = 0, r, k = 0;
    for (auto i = 1; i <= n; ++i) {
      cin >> r; diffs[r - rprev].push_back(i); k = max(k, r - rprev);
      rprev = r;
    }
    if (!check(k)) ++k;
    cout << "Case " << t << ": " << k << '\n';
  }
}
