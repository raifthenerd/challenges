#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N;
  cin >> N;
  for (auto i = 0; i < N; ++i) {
    string S;
    cin >> S;
    if (S == "1" || S == "4" || S == "78") cout << "+\n";
    else if (S.back() == '5') cout << "-\n";
    else if (S.front() == '9' && S.back() == '4') cout << "*\n";
    else if (S.substr(0, 3) == "190") cout << "?\n";
  }
}
