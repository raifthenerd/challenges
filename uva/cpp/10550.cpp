#include <bits/stdc++.h>

using namespace std;

const int base = 3 * 360;
const int unit = 9;
const int n = 40;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int init, first, second, third;
  while (true) {
    cin >> init >> first >> second >> third;
    if ((init | first | second | third) == 0) break;
    int result = base;
    int tmp = 0;
    tmp = (init - first) % n;
    if (tmp < 0) tmp += n;
    result += unit * tmp;
    tmp = (second - first) % n;
    if (tmp < 0) tmp += n;
    result += unit * tmp;
    tmp = (second - third) % n;
    if (tmp < 0) tmp += n;
    result += unit * tmp;
    cout << result << "\n";
  }
}
