#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto i = 1; i <= T; ++i) {
    cout << "Case " << i << ": ";
    int N; cin >> N;
    int c, s = 0;
    for (auto j = 0; j < N; ++j) {
      cin >> c;
      if (c > s) s = c;
    }
    cout << s << "\n";
  }
}
