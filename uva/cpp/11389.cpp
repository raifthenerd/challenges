#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int n, d, r; cin >> n >> d >> r; if (n == 0 && d == 0 && r == 0) break;
    vector<int> morning(n, 0), evening(n, 0);
    for (auto i = 0; i < n; ++i) cin >> morning[i];
    sort(morning.begin(), morning.end());
    for (auto i = 0; i < n; ++i) cin >> evening[i];
    sort(evening.begin(), evening.end(), greater<int>());
    auto result = 0;
    for (auto i = 0; i < n; ++i) {
      auto tmp = morning[i] + evening[i];
      result += (tmp > d) ? r * (tmp - d) : 0;
    }
    cout << result << '\n';
  }
}
