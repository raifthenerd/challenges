#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T; cin.ignore(1, '\n');
  while (T--) {
    string s; getline(cin, s);
    auto l = (s.size() - 2) / 3; int diff = 0;
    if (l > 0) {
      for (auto i = 0; i <= l; ++i) {
        auto tmp = s.substr(3 * i, 2);
        if (tmp == "MM") ++diff;
        else if (tmp == "FF") --diff;
      }
    }
    if (l == 0 || diff != 0) cout << "NO ";
    cout << "LOOP\n";
  }
}
