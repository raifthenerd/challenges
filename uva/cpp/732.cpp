#include <bits/stdc++.h>

using namespace std;

void dfs(string& orig, string& target, stack<char>& st,
         size_t idx_orig, size_t idx_target, char out, string& result) {
  if (out && out != target[idx_target]) return;
  else if (idx_target == target.size() - 1) {
    cout << result << '\n'; return;
  }
  result.push_back(' ');
  if (idx_orig < orig.size()) {
    result.push_back('i'); st.push(orig[idx_orig]);
    dfs(orig, target, st, idx_orig + 1, idx_target, out, result);
    st.pop(); result.pop_back();
  }
  if (!st.empty()) {
    result.push_back('o'); auto tmp = st.top(); st.pop();
    dfs(orig, target, st, idx_orig, idx_target + 1, tmp, result);
    st.push(tmp); result.pop_back();
  }
  result.pop_back();
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  string orig, target;
  while (cin >> orig) {
    cin >> target;
    cout << "[\n";
    if (orig.size() == target.size() &&
        is_permutation(orig.begin(), orig.end(), target.begin())) {
      stack<char> st; st.push(orig.front());
      string result = "i";
      dfs(orig, target, st, 1, -1, 0, result);
    }
    cout << "]\n";
  }
}
