#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();
int P; vii outpost; vector<vi> graph; vector<bool> taken; priority_queue<ii> pq;

int distSq(const ii& a, const ii& b) {
  auto dx = a.first - b.first, dy = a.second - b.second;
  return dx * dx + dy * dy;
}

void process(int u) {
  taken[u] = true;
  for (auto i = 0; i < P; ++i)
    if (!taken[i]) pq.push({-graph[u][i], i});
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  cout << fixed << setprecision(2);
  int N; cin >> N;
  while (N--) {
    int S; cin >> S >> P;
    outpost.clear();
    for (auto i = 0; i < P; ++i) {
      int x, y; cin >> x >> y; outpost.push_back({x, y});
    }
    graph.clear(); graph.assign(P, {}); for (auto& c : graph) c.assign(P, 0);
    for (auto i = 0; i < P; ++i)
      for (auto j = i + 1; j < P; ++j)
        graph[i][j] = graph[j][i] = distSq(outpost[i], outpost[j]);
    taken.clear(); taken.assign(P, false); process(0);
    vi result;
    while (!pq.empty()) {
      auto tmp = pq.top(); pq.pop();
      auto u = tmp.second, w = -tmp.first;
      if (!taken[u]) result.push_back(w), process(u);
    }
    sort(ALL(result));
    while (--S > 0) result.pop_back();
    cout << sqrt(result.back()) << '\n';
  }
}
