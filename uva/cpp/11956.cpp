#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << uppercase << setfill('0');
  int T; cin >> T;
  for (auto i = 1; i <= T; ++i) {
    cout << "Case " << dec << i << ':';
    uint_fast8_t mem[100] = {}, ptr = 0;
    string bf; cin >> bf;
    for (auto&& c : bf) {
      switch (c) {
        case '>': ptr = (ptr + 1) % 100; break;
        case '<': ptr = (ptr == 0 ? 99 : ptr - 1); break;
        case '+': ++mem[ptr]; break;
        case '-': --mem[ptr]; break;
        default: continue;
      }
    }
    for (unsigned int m : mem)
      cout << ' ' << setw(2) << hex << m;
    cout << '\n';
  }
}
