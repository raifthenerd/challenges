#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t N; cin >> N;
  int cumsum[N][N];
  for (size_t i = 0; i < N; ++i) {
    for (size_t j = 0; j < N; ++j) {
      cin >> cumsum[i][j];
      if (i > 0) cumsum[i][j] += cumsum[i-1][j];
      if (j > 0) cumsum[i][j] += cumsum[i][j-1];
      if (i > 0 && j > 0) cumsum[i][j] -= cumsum[i-1][j-1];
    }
  }
  int result = numeric_limits<int>::min();
  for (size_t i = 0; i < N; ++i) for (size_t x = i; x < N; ++x) {
    for (size_t j = 0; j < N; ++j) for (size_t y = j; y < N; ++y) {
      auto tmp = cumsum[x][y];
      if (i > 0) tmp -= cumsum[i-1][y];
      if (j > 0) tmp -= cumsum[x][j-1];
      if (i > 0 && j > 0) tmp += cumsum[i-1][j-1];
      result = max(result, tmp);
    }
  }
  cout << result << '\n';
}
