#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T;
  while (++T) {
    int n; cin >> n; if (n == 0) break;
    cout << "Case " << T << ":\n";
    vector<int> xs; while (n--) { int x; cin >> x; xs.push_back(x); }
    set<int> sums;
    for (auto x : xs) {
      for (auto y : xs) {
        if (x != y) sums.insert(x+y);
      }
    }
    int m; cin >> m;
    while (m--) {
      int q, res; cin >> q;
      cout << "Closest sum to " << q << " is ";
      auto u = lower_bound(sums.begin(), sums.end(), q);
      if (u == sums.begin()) res = *u;
      else {
        auto l = prev(u);
        if (u == sums.end()) res = *l;
        else res = (*u - q < q - *l) ? *u : *l;
      }
      cout << res << ".\n";
    }
  }
}
