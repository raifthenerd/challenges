#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 2;
double grid[21][20][20];
int p[21][20][20];

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T = 0;
  int n; while (cin >> n) { ++T;
    REP(i, 0, n) {
      REP(j, 0, n) {
        p[0][i][j] = i;
        REP(k, 0, n) grid[k][i][j] = -oo;
        if (i == j) continue;
        double tmp; cin >> tmp; grid[0][i][j] = log(tmp);
      }
      grid[0][i][i] = 0.0;
    }
    bool found = false;
    REP(k, 1, n) {
      REP(i, 0, n) REP(j, 0, n) REP(l, 0, n)
        if (grid[k][i][j] < grid[k-1][i][l] + grid[0][l][j]) {
          grid[k][i][j] = grid[k-1][i][l] + grid[0][l][j];
          p[k][i][j] = p[0][l][j];
        }
      REP(i, 0, n) {
        if (grid[k][i][i] > log(1.01)) {
          found = true; auto j = p[k][i][i];
          cout << i+1;
          stack<int> s; s.push(i+1);
          while (k--) s.push(j+1), j = p[k][i][j];
          while(!s.empty()) cout << ' ' << s.top(), s.pop();
          cout << '\n';
          break;
        }
      }
      if (found) break;
    }
    if (!found) cout << "no arbitrage sequence exists\n";
  }
}
