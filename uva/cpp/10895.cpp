#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int m, n;
  while (cin >> m) {
    cin >> n; cout << n << ' ' << m << '\n';
    map<int, list<pair<int, int>>> graph;
    for (auto row = 1; row <= m; ++row) {
      queue<int> cols;
      int r; cin >> r;
      while (r--) {
        int col; cin >> col; cols.push(col);
      }
      while (!cols.empty()) {
        int val; cin >> val;
        graph[cols.front()].push_back({row, val});
        cols.pop();
      }
    }
    for (auto col = 1; col <= n; ++col) {
      cout << graph[col].size();
      for (auto& c : graph[col])
        cout << ' ' << c.first;
      cout << '\n';
      bool first = true;
      for (auto& c : graph[col]) {
        if (!first) cout << ' ';
        else first = false;
        cout << c.second;
      }
      cout << '\n';
    }
  }
}
