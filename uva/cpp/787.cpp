#include <bits/stdc++.h>

using namespace std;

class BigInt {
 private:
  static const unsigned long long base = 1000000000;
  static const size_t length = 56;
  int sign; unsigned long long digits[length];
 public:
  BigInt(int n = 0) {
    if (n > 0) sign = 1;
    else if (n < 0) sign = -1;
    else sign = 0;
    digits[0] = abs(n);
    for (size_t i = 1; i < length; ++i) digits[i] = 0;
  }
  BigInt& operator=(const BigInt& other) {
    if (this != &other) {
      sign = other.sign;
      for (size_t i = 0; i < length; ++i)
        digits[i] = other.digits[i];
    }
    return *this;
  }
  BigInt& operator*=(const BigInt& other) {
    if (this != &other) {
      sign *= other.sign;
      unsigned long long tmp[length] = {};
      for (size_t i = 0; i < length; ++i) {
        unsigned long long carry = 0;
        if (sign == 0) {
          digits[i] = 0;
        } else {
          unsigned long long carry = 0;
          for (size_t j = 0; j < length - i; ++j) {
            carry += tmp[i+j] + digits[j] * other.digits[i];
            tmp[i+j] = carry%base;
            carry /= base;
          }
        }
      }
      for (size_t i = 0; i < length; ++i) digits[i] = tmp[i];
    }
    return *this;
  }
  BigInt& operator*=(const int other) {
    if (other == 0) sign = 0;
    else if (other < 0) sign *= -1;
    unsigned long long carry = 0;
    for (size_t i = 0; i < length; ++i) {
      if (sign == 0) digits[i] = 0;
      else {
        carry += digits[i] * abs(other);
        digits[i] = carry%base;
        carry /= base;
      }
    }
    return *this;
  }
  const BigInt operator*(const BigInt& other) const {
    return BigInt(*this) *= other;
  }
  const BigInt operator*(const int other) const {
    return BigInt(*this) *= other;
  }
  bool operator<(const BigInt& other) {
    if (sign < other.sign) return true;
    if (sign > other.sign) return false;
    if (sign == 0) return false;
    for (size_t i = length; i > 0; --i) {
      if (digits[i-1] < other.digits[i-1])
        return sign > 0 ? true : false;
      if (digits[i-1] > other.digits[i-1])
        return sign > 0 ? false : true;
    }
    return false;
  }
  bool operator>(const BigInt& other) {
    if (sign < other.sign) return false;
    if (sign > other.sign) return true;
    if (sign == 0) return false;
    for (size_t i = length; i > 0; --i) {
      if (digits[i-1] > other.digits[i-1])
        return sign > 0 ? true : false;
      if (digits[i-1] < other.digits[i-1])
        return sign > 0 ? false : true;
    }
    return false;
  }
  string to_str() const {
    if (sign == 0) return "0";
    string s;
    bool prefix = true;
    for (auto i = length; i > 0; --i) {
      if (prefix) {
        if (digits[i-1] != 0) {
          s = to_string(digits[i-1]);
          prefix = false;
        }
      } else {
        if (digits[i-1] != 0) {
          auto tmp = to_string(digits[i-1]);
          s += string(9 - tmp.size(), '0') + tmp;
        } else {
          s += string(9, '0');
        }
      }
    }
    if (sign < 0) s = '-' + s;
    return s;
  }
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  BigInt pre_pos = 1, pre_neg = 1, body = 1, suf_neg = 1, suf_pos = 1, result = -999999;
  bool pre_valid = false, body_valid = false, suf_valid = false;
  int n_neg = 0, x;
  while (cin >> x) {
    if (result < -999998) result = x;
    if (x == 0 || x == -999999) {
      BigInt tmp;
      if (suf_neg > 0) {
        if (body < 0) tmp = pre_pos * pre_neg * body;
        else if (body_valid) {
          if (pre_valid && pre_pos > body) tmp = pre_pos;
          else tmp = body;
        } else {
          if (pre_valid) tmp = pre_pos;
          else if (pre_neg < 0) tmp = pre_neg;
        }
      } else {
        if (body > 0) tmp = pre_pos * pre_neg * body * suf_neg * suf_pos;
        else {
          auto pre = pre_pos * pre_neg, suf = suf_neg * suf_pos;
          if (pre < suf) tmp = pre * body;
          else tmp = suf * body;
        }
      }
      if (tmp > result) result = tmp;
      if (x == 0 && result < 0) result = 0;
      if (x == -999999) {
        cout << result.to_str() << '\n';
        result = -999999;
      }
      pre_pos = pre_neg = body = suf_neg = suf_pos = 1;
      pre_valid = body_valid = suf_valid = false; n_neg = 0;
    } else if (x < 0) {
      if (n_neg == 0) {
        pre_neg = x;
      } else if (n_neg == 1) {
        suf_neg = x;
      } else {
        body *= suf_neg * suf_pos;
        suf_neg = x;
        suf_pos = 1;
        body_valid = true;
        suf_valid = false;
      }
      ++n_neg;
    } else {
      if (n_neg == 0) {
        pre_pos *= x; pre_valid = true;
      } else if (n_neg == 1) {
        body *= x; body_valid = true;
      } else {
        suf_pos *= x; suf_valid = true;
      }
    }
  }
}
