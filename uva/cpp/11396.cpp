#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int V; cin >> V; if (V == 0) break;
    set<int> adj[V]; bool color[V], visited[V];
    for (auto i = 0; i < V; ++i) visited[i] = false;
    while (true) {
      int a, b; cin >> a >> b; if (a == 0 && b == 0) break;
      adj[a-1].insert(b-1); adj[b-1].insert(a-1);
    }
    bool decomp = V%2 == 0; int t = 0, f = 0;
    queue<int> q;
    for (auto i = 0; decomp && i < V; ++i) {
      if (visited[i]) continue;
      color[i] = true; q.push(i); visited[i] = true;
      while (decomp && !q.empty()) {
        auto u = q.front(); q.pop(); if (color[u]) ++t; else ++f;
        for (auto c : adj[u]) {
          if (!visited[c]) {
            color[c] = !color[u]; q.push(c); visited[c] = true;
          } else if (color[c] == color[u]) decomp = false;
        }
      }
    }
    decomp = decomp && t == V/2 && f == V/2;
    cout << (decomp ? "YES\n" : "NO\n");
  }
}
