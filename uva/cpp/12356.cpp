#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int S, B; cin >> S >> B;
    if (S == 0 && B == 0) break;
    vector<int> ls, rs;
    for (auto i = 0; i <= S; ++i) {
      ls.push_back(i-1);
      rs.push_back(i+1);
    }
    for (auto i = 0; i < B; ++i) {
      int L, R; cin >> L >> R;
      ls[L] == 0 ? (cout << '*') : (cout << ls[L]); cout << ' ';
      rs[R] == S+1 ? (cout << '*') : (cout << rs[R]); cout << '\n';
      ls[rs[R]] = ls[L];
      rs[ls[L]] = rs[R];
    }
    cout << "-\n";
  }
}
