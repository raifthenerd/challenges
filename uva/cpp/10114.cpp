#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int dur, depre, month;
    double downpay, debt, rate;
    cin >> dur >> downpay >> debt >> depre;
    if (dur < 0) break;
    queue<int> months;
    queue<double> rates;
    for (auto i = 0; i < depre; ++i) {
      cin >> month >> rate;
      months.push(month);
      rates.push(1.0 - rate);
    }
    double car = downpay + debt, tmp = debt / dur;
    for (month = 0; month <= dur; ++month) {
      if (month == months.front()) {
        rate = rates.front(); months.pop(); rates.pop();
      }
      car *= rate;
      if (car > debt) {
        cout << month << " month" << (month == 1 ? "" : "s") << "\n";
        break;
      }
      debt -= tmp;
    }
  }
}
