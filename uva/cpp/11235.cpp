#include <bits/stdc++.h>

using namespace std;

template<class T, class Compare = less<T>>
class SegmentTree {
 private:
  vector<T> mem;
  vector<size_t> seg;
  Compare comp;
  size_t left(size_t p) { return p << 1; }
  size_t right(size_t p) { return 1 + (p << 1); }
  void build(size_t p, size_t L, size_t R) {
    if (L == R) seg[p] = L;
    else {
      build(left(p), L, (L + R) / 2);
      build(right(p), (L + R) / 2 + 1, R);
      auto pl = seg[left(p)], pr = seg[right(p)];
      seg[p] = comp(mem[pl], mem[pr]) ? pl : pr;
    }
  };
  size_t range_query(size_t p, size_t L, size_t R, size_t i, size_t j) {
    if (i > R || j < L) return numeric_limits<size_t>::max();
    if (i <= L && j >= R) return seg[p];
    auto pl = range_query(left(p), L, (L + R) / 2, i, j),
         pr = range_query(right(p), (L + R) / 2 + 1, R, i, j);
    if (pl == numeric_limits<size_t>::max()) return pr;
    if (pr == numeric_limits<size_t>::max()) return pl;
    return comp(mem[pl], mem[pr]) ? pl : pr;
  };
 public:
  SegmentTree(const vector<T> &_mem) {
    mem = _mem;
    seg.assign(4*mem.size(), 0);
    build(1, 0, mem.size()-1);
  };
  size_t range_query(size_t i, size_t j) {
    return mem[range_query(1, 0, mem.size()-1, i, j)];
  };
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    size_t n, q; cin >> n; if (n == 0) break; cin >> q;
    vector<size_t> freqs, ub, lb;
    size_t prev = 1; int val; cin >> val;
    for (size_t curr = 2; curr <= n; ++curr) {
      int tmp; cin >> tmp;
      if (tmp != val) {
        for (auto i = prev; i < curr; ++i) {
          freqs.push_back(curr-prev);
          ub.push_back(curr); lb.push_back(prev-1);
        }
        val = tmp;
        prev = curr;
      }
    }
    for (auto i = prev; i < n + 1; ++i) {
      freqs.push_back(n-prev+1);
      ub.push_back(n+1); lb.push_back(prev-1);
    }
    SegmentTree<size_t, greater<size_t>> st(freqs);
    while (q--) {
      size_t i, j, result; cin >> i >> j;
      if (i > lb[j-1] && j < ub[i-1]) {
        result = j-i+1;
      } else {
        result = max(ub[i-1]-i, j-lb[j-1]);
        if (ub[i-1] <= lb[j-1])
        result = max(result, st.range_query(ub[i-1]-1, lb[j-1]-1));
      }
      cout << result << '\n';
    }
  }
}
