#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  auto b = false;
  char c;
  while (cin.get(c)) {
    if (c == '"') {
      c = b ? '\'' : '`';
      b = !b;
      cout.put(c);
    }
    cout.put(c);
  }
}
