#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 2;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int N; cin >> N; if (N == 0) break;
    set<char> vertices;
    map<char, vector<pair<char, int>>> graph[2];
    while (N--) {
      char age, dir, X, Y; int e; cin >> age >> dir >> X >> Y >> e;
      vertices.insert(X), vertices.insert(Y);
      bool old = age == 'M';
      graph[old][X].push_back({Y, e});
      if (dir == 'B') graph[old][Y].push_back({X, e});
    }
    char nodes[2]; cin >> nodes[0] >> nodes[1];
    map<char, int> dist[2];
    TRAV(v, vertices) dist[0][v] = dist[1][v] = oo;
    priority_queue<pair<int, char>,
                   vector<pair<int, char>>,
                   greater<pair<int, char>>> pq;
    REP(i, 0, 2) {
      pq.push({dist[i][nodes[i]] = 0, nodes[i]});
      while (!pq.empty()) {
        auto d = pq.top().first; auto u = pq.top().second; pq.pop();
        if (d > dist[i][u]) continue;
        TRAV(v, graph[i][u])
          if (dist[i][v.first] > dist[i][u] + v.second)
            pq.push({dist[i][v.first] = dist[i][u] + v.second, v.first});
      }
    }
    set<char> meetings; int result = oo;
    TRAV(v, vertices)
      if (dist[0][v] + dist[1][v] < result) {
        result = dist[0][v] + dist[1][v]; meetings.clear(); meetings.insert(v);
      } else if (dist[0][v] + dist[1][v] == result) meetings.insert(v);
    if (result < oo) {
      cout << result; TRAV(m, meetings) cout << ' ' << m; cout << '\n';
    } else cout << "You will never meet.\n";
  }
}
