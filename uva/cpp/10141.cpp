#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T = 0;
  while (++T) {
    int n, p; cin >> n >> p;
    if (n == 0 && p == 0) break;
    cin.ignore(1, '\n');
    string result; double price = 0.0; int requirements = -1;
    string s; double d; int r;
    while (n--) cin.ignore(82, '\n');
    while (p--) {
      getline(cin, s);
      cin >> d >> r; cin.ignore(1, '\n');
      if (requirements < r) {
        price = d;
        requirements = r;
        result = s;
      } else if (requirements == r && price > d) {
        price = d;
        result = s;
      }
      while (r--) cin.ignore(82, '\n');
    }
    if (T > 1) cout << '\n';
    cout << "RFP #" << T << '\n' << result << '\n';
  }
}
