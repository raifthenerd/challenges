#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t N, L, U;
  while (cin >> N) { cin >> L >> U;
    if (L == U) {
      cout << L << '\n'; continue;
    }
    int idx = 31; size_t mask = 1U << idx;
    while ((L & mask) == (U & mask)) mask = 1U << --idx;
    if (idx == 31) mask = 0;
    else mask = ~((1U << (idx + 1)) - 1);
    size_t M = L & mask;
    mask = (1U << idx) - 1; M |= ~N & mask;
    if (~N & (1U << idx)) {
      M |= 1U << idx;
      while (M > U && idx != 0) {
        mask = 1U << --idx;
        if ((M & mask) != (U & mask)) M &= ~mask;
      }
    } else {
      while (M < L && idx != 0) {
        mask = 1U << --idx;
        if ((M & mask) != (L & mask)) M |= mask;
      }
    }
    cout << M << '\n';
  }
}
