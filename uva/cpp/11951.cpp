#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t T; cin >> T;
  for (size_t t = 1; t <= T; ++t) {
    size_t N, M; long long K; cin >> N >> M >> K;
    long long P[N][M];
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < M; ++j) {
        cin >> P[i][j];
        if (i > 0) P[i][j] += P[i-1][j];
        if (j > 0) P[i][j] += P[i][j-1];
        if (i > 0 && j > 0) P[i][j] -= P[i-1][j-1];
      }
    }
    size_t area = 0; long long price = K;
    for (size_t i = 0; i < N; ++i) for (size_t x = i; x < N; ++x) {
      for (size_t j = 0; j < M; ++j) for (size_t y = j; y < M; ++y) {
        auto s = (x+1-i)*(y+1-j);
        auto p = P[x][y];
        if (i > 0) p -= P[i-1][y];
        if (j > 0) p -= P[x][j-1];
        if (i > 0 && j > 0) p += P[i-1][j-1];
        if (s > area && p <= K) {
          area = s; price = p;
        } else if (s == area && p < price) price = p;
      }
    }
    cout << "Case #" << t << ": " << area << ' ' << price << '\n';
  }
}
