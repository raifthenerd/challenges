#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    size_t N; cin >> N;
    int cumsum[2*N][2*N];
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
        cin >> cumsum[i][j];
        if (i > 0) cumsum[i][j] += cumsum[i-1][j];
        if (j > 0) cumsum[i][j] += cumsum[i][j-1];
        if (i > 0 && j > 0) cumsum[i][j] -= cumsum[i-1][j-1];
        cumsum[i+N][j+N] = cumsum[i][j+N] = cumsum[i+N][j] = cumsum[i][j];
      }
    }
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < N; ++j) {
        cumsum[i+N][j] += cumsum[N-1][j];
        cumsum[i][j+N] += cumsum[i][N-1];
        cumsum[i+N][j+N] += cumsum[N-1][j] + cumsum[i][N-1] + cumsum[N-1][N-1];
      }
    }
    int result = numeric_limits<int>::min();
    for (size_t i = 0; i < N; ++i) for (size_t x = i; x < i + N; ++x) {
      for (size_t j = 0; j < N; ++j) for (size_t y = j; y < j + N; ++y) {
        auto tmp = cumsum[x][y];
        if (i > 0) tmp -= cumsum[i-1][y];
        if (j > 0) tmp -= cumsum[x][j-1];
        if (i > 0 && j > 0) tmp += cumsum[i-1][j-1];
        result = max(result, tmp);
      }
    }
    cout << result << '\n';
  }
}
