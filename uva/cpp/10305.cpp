#include <bits/stdc++.h>

using namespace std;

vector<vector<int>> adj; vector<bool> visited;

string DFS(int i) {
  if (visited[i]) return ""; visited[i] = true;
  string s = "";
  for (auto j : adj[i]) if (!visited[j]) s += DFS(j);
  return s + to_string(i+1) + ' ';
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int m, n; cin >> n >> m; if (n == 0 && m == 0) break;
    adj.assign(n, {}); visited.assign(n, false);
    while (m--) {
      int i, j; cin >> i >> j; adj[j-1].push_back(i-1);
    }
    string s = "";
    for (auto i = 0; i < n; ++i) s += DFS(i); s.pop_back();
    cout << s << '\n';
  }
}
