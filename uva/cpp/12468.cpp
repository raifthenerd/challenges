#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int a, b; cin >> a >> b;
    if (a == -1 && b == -1) break;
    int tmp = abs(a - b);
    cout << min(tmp, 100 - tmp) << "\n";
  }
}
