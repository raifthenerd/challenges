#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

vector<vi> graph;
vi dfs_num, dfs_low, dfs_parent;
vector<bool> articulations;
int counter, rootChildren, root;

void articulationPoint(int node) {
  dfs_low[node] = dfs_num[node] = ++counter;
  for (auto c : graph[node]) {
    if (dfs_num[c] == oo) {
      dfs_parent[c] = node;
      if (node == root) ++rootChildren;
      articulationPoint(c);
      if (dfs_low[c] >= dfs_num[node]) articulations[node] = true;
      dfs_low[node] = min(dfs_low[node], dfs_low[c]);
    } else if (c != dfs_parent[node])
      dfs_low[node] = min(dfs_low[node], dfs_num[c]);
  }
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int N; cin >> N; if (N == 0) break;
    graph.clear(); graph.assign(N, {});
    while (true) {
      int np; cin >> np; if (np == 0) break;
      while (cin.get() != '\n') {
        int tmp; cin >> tmp;
        graph[tmp-1].push_back(np-1); graph[np-1].push_back(tmp-1);
      }
    }
    dfs_num.clear(); dfs_num.assign(N, oo);
    dfs_low.clear(); dfs_low.assign(N, 0);
    dfs_parent.clear(); dfs_parent.assign(N, 0);
    articulations.clear(); articulations.assign(N, false);
    counter = 0;
    int criticals = 0;
    for (auto i = 0; i < N; ++i) {
      if (dfs_num[i] == oo) {
        root = i; rootChildren = 0; articulationPoint(root);
        articulations[root] = rootChildren > 1;
      }
    }
    for (auto c : articulations) if (c) ++criticals;
    cout << criticals << '\n';
  }
}
