#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int N; cin >> N;
  for (auto x = 1; x <= N; ++x) {
    int n, m, S, T; cin >> n >> m >> S >> T;
    vector<vii> graph; graph.assign(n, {});
    while (m--) {
      int a, b, w; cin >> a >> b >> w;
      graph[a].push_back({w, b}); graph[b].push_back({w, a});
    }
    vi dist; dist.assign(n, oo >> 1); dist[S] = 0;
    priority_queue<ii, vii, greater<ii>> pq; pq.push({dist[S], S});
    while (!pq.empty()) {
      auto w = pq.top().first, u = pq.top().second; pq.pop();
      if (w > dist[u]) continue;
      TRAV(v, graph[u]) {
        if (dist[u] + v.first < dist[v.second]) {
          dist[v.second] = dist[u] + v.first;
          pq.push({dist[v.second], v.second});
        }
      }
    }
    cout << "Case #" << x << ": ";
    if (dist[T] == (oo >> 1)) cout << "unreachable\n";
    else cout << dist[T] << '\n';
  }
}
