#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int N, n; cin >> N >> n;
    if (N == 0 && n == 0) break;
    int result[4] = {0, 0, 0, 0};
    vector<string> board;
    for (auto i = 0; i < N; ++i) {
      string tmp; cin >> tmp; board.push_back(tmp);
    }
    vector<string> pattern;
    for (auto i = 0; i < n; ++i) {
      string tmp; cin >> tmp; pattern.push_back(tmp);
    }
    for (auto rot = 0; rot < 4; ++rot) {
      vector<string> pat;
      for (auto i = 0; i < n; ++i) {
        string tmp;
        for (auto j = 0; j < n; ++j) {
          int foo, bar;
          switch (rot) {
            case 0:
              foo = i; bar = j;
              break;
            case 1:
              foo = n-1-j; bar = i;
              break;
            case 2:
              foo = n-1-i; bar = n-1-j;
              break;
            case 3:
              foo = j; bar = n-1-i;
              break;
          }
          tmp.push_back(pattern[foo][bar]);
        }
        pat.push_back(tmp);
      }
      for (auto i = 0; i <= N-n; ++i) {
        int loc = 0, j = 0;
        while ((loc = board[i].find(pat[j], loc)) != string::npos) {
          bool found = true;
          for (j = 1; j < n; ++j) {
            auto l = board[i+j].find(pat[j], loc);
            if (loc != l) {
              found = false; break;
            }
          }
          if (found) ++result[rot];
          j = 0; ++loc;
        }
      }
    }
    cout << result[0] << ' '
         << result[1] << ' '
         << result[2] << ' '
         << result[3] << '\n';
  }
}
