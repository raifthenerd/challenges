#include <bits/stdc++.h>

using namespace std;

unordered_map<size_t, unordered_set<size_t>> adj;
vector<bool> blacks, result;
vector<int> visited;
size_t n_blacks;

void bt(size_t curr) {
  if (curr == blacks.size()) {
    size_t tmp = 0;
    for (auto b : blacks) tmp += b;
    if (tmp > n_blacks) {
      n_blacks = tmp;
      result = blacks;
    }
  } else if (visited[curr] > 0) bt(curr + 1);
  else {
    ++visited[curr];
    blacks[curr] = true;
    for (auto next : adj[curr]) {
      ++visited[next];
      blacks[next] = false;
    }
    bt(curr + 1);
    for (auto next : adj[curr]) --visited[next];
    blacks[curr] = false;
    bt(curr + 1);
    --visited[curr];
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int m; cin >> m;
  while (m--) {
    n_blacks = 0;
    size_t n, k; cin >> n >> k;
    blacks.clear(); blacks.assign(n, false);
    visited.clear(); visited.assign(n, 0);
    adj.clear(); while (k--) {
      size_t x, y; cin >> x >> y;
      adj[x-1].insert(y-1);
      adj[y-1].insert(x-1);
    }
    bt(0);
    cout << n_blacks << '\n';
    bool prefix = true;
    for (size_t i = 0; i < n; ++i) {
      if (result[i]) {
        if (prefix) prefix = false;
        else cout << ' ';
        cout << i+1;
      }
    }
    cout << '\n';
  }
}
