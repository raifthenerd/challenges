#include <bits/stdc++.h>

using namespace std;

int M, N;
char world[20][20]; bool visited[20][20];

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (cin >> M) { cin >> N;
    for (auto y = 0; y < M; ++y) {
      for (auto x = 0; x < N; ++x) {
        cin >> world[y][x]; visited[y][x] = false;
      }
    }
    int ky, kx; cin >> ky >> kx; char land = world[ky][kx];
    queue<pair<int, int>> q; int result = 0;
    for (auto y = 0; y < M; ++y) {
      for (auto x = 0; x < N; ++x) {
        if (world[y][x] != land || visited[y][x]) continue;
        bool update = true; int tmp = 0;
        q.push({y, x}); visited[y][x] = true;
        while (!q.empty()) {
          auto j = q.front().first, i = q.front().second; q.pop(); ++tmp;
          if (j == ky && i == kx) update = false;
          if (!visited[j][(i+N-1)%N] && world[j][(i+N-1)%N] == land) {
            q.push({j, (i+N-1)%N}); visited[j][(i+N-1)%N] = true;
          }
          if (!visited[j][(i+1)%N] && world[j][(i+1)%N] == land) {
            q.push({j, (i+1)%N}); visited[j][(i+1)%N] = true;
          }
          if (j > 0 && !visited[j-1][i] && world[j-1][i] == land) {
            q.push({j-1, i}); visited[j-1][i] = true;
          }
          if (j + 1 < M && !visited[j+1][i] && world[j+1][i] == land) {
            q.push({j+1, i}); visited[j+1][i] = true;
          }
        }
        if (update) result = max(result, tmp);
      }
    }
    cout << result << '\n';
  }
}
