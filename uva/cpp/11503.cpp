#include <bits/stdc++.h>

using namespace std;

template<typename T>
class Partition {
 private:
  unordered_map<T, T> parent;
  unordered_map<T, size_t> rank, n_elems;
  size_t n_partitions;
  T representative(T e) {
    return (parent[e] == e) ? e : (parent[e] = representative(parent[e]));
  };
 public:
  Partition() {
    parent.clear(); rank.clear(); n_elems.clear(); n_partitions = 0;
  };
  size_t numberOfPartitions() { return n_partitions; }
  void addElement(T e) {
    if (parent.count(e) == 0 && rank.count(e) == 0 && n_elems.count(e) == 0) {
      parent[e] = e; rank[e] = 0; n_elems[e] = 1; ++n_partitions;
    }
  };
  size_t sizeOfSet(T e) {
    return n_elems[representative(e)];
  };
  bool isSameSets(T e1, T e2) {
    return representative(e1) == representative(e2);
  };
  void unionSets(T e1, T e2) {
    if (!isSameSets(e1, e2)) {
      e1 = representative(e1); e2 = representative(e2);
      if (rank[e1] > rank[e2]) {
        parent[e2] = e1;
        n_elems[e1] += n_elems[e2];
      } else {
        if (rank[e1] == rank[e2]) ++rank[e2];
        parent[e1] = e2;
        n_elems[e2] += n_elems[e1];
      }
      --n_partitions;
    }
  };
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int F; cin >> F;
    Partition<string> network;
    while (F--) {
      string foo, bar; cin >> foo >> bar;
      network.addElement(foo);
      network.addElement(bar);
      network.unionSets(foo, bar);
      cout << network.sizeOfSet(foo) << '\n';
    }
  }
}
