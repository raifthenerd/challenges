#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    size_t A, B, C; cin >> A >> B >> C;
    long long cumsum[A][B][C];
    for (size_t i = 0; i < A; ++i) {
      for (size_t j = 0; j < B; ++j) {
        for (size_t k = 0; k < C; ++k) {
          cin >> cumsum[i][j][k];
          if (i > 0) cumsum[i][j][k] += cumsum[i-1][j][k];
          if (j > 0) cumsum[i][j][k] += cumsum[i][j-1][k];
          if (k > 0) cumsum[i][j][k] += cumsum[i][j][k-1];
          if (i > 0 && j > 0) cumsum[i][j][k] -= cumsum[i-1][j-1][k];
          if (j > 0 && k > 0) cumsum[i][j][k] -= cumsum[i][j-1][k-1];
          if (k > 0 && i > 0) cumsum[i][j][k] -= cumsum[i-1][j][k-1];
          if (i > 0 && j > 0 && k > 0) cumsum[i][j][k] += cumsum[i-1][j-1][k-1];
        }
      }
    }
    long long result = numeric_limits<long long>::min();
    for (size_t i = 0; i < A; ++i) for (size_t x = i; x < A; ++x) {
      for (size_t j = 0; j < B; ++j) for (size_t y = j; y < B; ++y) {
        for (size_t k = 0; k < C; ++k) for (size_t z = k; z < C; ++z) {
          auto tmp = cumsum[x][y][z];
          if (i > 0) tmp -= cumsum[i-1][y][z];
          if (j > 0) tmp -= cumsum[x][j-1][z];
          if (k > 0) tmp -= cumsum[x][y][k-1];
          if (i > 0 && j > 0) tmp += cumsum[i-1][j-1][z];
          if (j > 0 && k > 0) tmp += cumsum[x][j-1][k-1];
          if (k > 0 && i > 0) tmp += cumsum[i-1][y][k-1];
          if (i > 0 && j > 0 && k > 0) tmp -= cumsum[i-1][j-1][k-1];
          result = max(result, tmp);
        }
      }
    }
    cout << result << '\n';
    if (T) cout << '\n';
  }
}
