#include <bits/stdc++.h>

using namespace std;

const string song[16] = {"Happy",
                         "birthday",
                         "to",
                         "you",
                         "Happy",
                         "birthday",
                         "to",
                         "you",
                         "Happy",
                         "birthday",
                         "to",
                         "Rujia",
                         "Happy",
                         "birthday",
                         "to",
                         "you"};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n; cin >> n;
  vector<string> names(n);
  for (auto i = 0; i < n; ++i) cin >> names[i];
  for (auto i = 0; i < 16 * (1 + ((n - 1) / 16)); ++i)
    cout << names[i % n] << ": " << song[i % 16] << '\n';
}
