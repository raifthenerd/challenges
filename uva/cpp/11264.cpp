#include <bits/stdc++.h>

using namespace std;

vector<long long> coins;
vector<map<long long, int>> cache;
int f(int idx, long long limit) {
  if (cache[idx].count(limit) == 1) return cache[idx][limit];
  if (limit <= 0) cache[idx][limit] = 0;
  else if (idx == 0) cache[idx][limit] = 1;
  else if (coins[idx] > limit)
    cache[idx][limit] = f(idx - 1, limit);
  else if (2 * coins[idx] <= limit + 1)
    cache[idx][limit] = 1 + f(idx - 1, coins[idx] - 1);
  else cache[idx][limit] = max(f(idx - 1, coins[idx] - 1), 1 + f(idx - 1, limit - coins[idx]));
  return cache[idx][limit];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    coins.clear(); cache.clear();
    while (n--) {
      long long c; cin >> c; coins.push_back(c); cache.push_back({});
    }
    cout << f(coins.size() - 1, 2 * coins.back() - 1) << '\n';
  }
}
