#include <bits/stdc++.h>

using namespace std;

int N;
vector<char> leds;
const vector<char> digits = {
  0b0111111,
  0b0000110,
  0b1011011,
  0b1001111,
  0b1100110,
  0b1101101,
  0b1111101,
  0b0000111,
  0b1111111,
  0b1101111
};

char get_mask(char digit, char led) {
  char candidate = (~(digit ^ led)) & 127;
  if ((digit & candidate) == led) return candidate;
  else return -1;
}
bool bt(int curr, char mask, int idx) {
  if (idx == N) return true;
  if (curr < 0) return false;
  char new_mask = get_mask(digits[curr] & mask, leds[idx]);
  return new_mask >= 0 && bt(curr-1, mask & new_mask, idx + 1);
}
bool bt() {
  for (auto d = 9; d >= 0; --d) {
    char mask = get_mask(digits[d], leds[0]);
    if (mask >= 0 && bt(d-1, mask, 1)) return true;
  }
  return false;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    cin >> N; if (N == 0) break;
    leds.clear(); leds.assign(N, 0);
    for (auto i = 0; i < N; ++i) {
      for (auto j = 0; j < 7; ++j) {
        char tmp; cin >> tmp; leds[i] |= (tmp == 'Y') << j;
      }
    }
    if (!bt()) cout << "MIS"; cout << "MATCH\n";
  }
}
