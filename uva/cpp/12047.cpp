#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 2;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T; cin >> T; while (T--) {
    int N, M, s, t, p; cin >> N >> M >> s >> t >> p; --s; --t;
    map<int, vii> outgoing, incoming;
    while (M--) {
      int u, v, c; cin >> u >> v >> c; --u; --v;
      if (c <= p)
        outgoing[u].push_back({v, c}), incoming[v].push_back({u, c});
    }
    vi dist[2]; REP(i, 0, 2) dist[i].assign(N, oo);
    priority_queue<ii, vii, greater<ii>> pq;
    pq.push({dist[0][s] = 0, s});
    while (!pq.empty()) {
      auto d = pq.top().first, u = pq.top().second; pq.pop();
      if (d > dist[0][u]) continue;
      TRAV(v, outgoing[u])
        if (dist[0][u] + v.second < dist[0][v.first])
          pq.push({dist[0][v.first] = dist[0][u] + v.second, v.first});
    }
    auto result = -1;
    pq.push({dist[1][t] = 0, t});
    while (!pq.empty()) {
      auto d = pq.top().first, u = pq.top().second; pq.pop();
      if (d > dist[1][u]) continue;
      TRAV(v, incoming[u]) {
        if (dist[0][v.first] + v.second + d <= p)
          result = max(result, v.second);
        if (dist[1][u] + v.second < dist[1][v.first])
          pq.push({dist[1][v.first] = dist[1][u] + v.second, v.first});
      }
    }
    cout << result << '\n';
  }
}
