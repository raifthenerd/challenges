#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t c; cin >> c;
  while (c--) {
    size_t l, m; cin >> l >> m; l *= 100;
    queue<size_t> left, right;
    auto *curr = &left, *other = &right;
    size_t trans = 0, cap = 0;
    while (m--) {
      size_t car; string dir; cin >> car >> dir;
      if (dir == "left") left.push(car);
      else right.push(car);
    }
    while (!curr->empty() || !other->empty()) {
      while (!curr->empty() && curr->front() + cap <= l) {
        cap += curr->front(); curr->pop();
      }
      swap(curr, other); ++trans; cap = 0;
    }
    cout << trans << '\n';
  }
}
