#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();
vector<string> vertices;
map<string, map<string, bool>> adj;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T = 0; while (++T) {
    int NC; cin >> NC; if (NC == 0) break;
    vertices.clear(); adj.clear();
    while (NC--) {
      int NE; cin >> NE;
      string e1, e2; cin >> e1; vertices.push_back(e1);
      while (--NE) {
        string e2; cin >> e2; vertices.push_back(e2);
        adj[e1][e2] = true; e1 = e2;
      }
    }
    int NM; cin >> NM;
    while (NM--) {
      string e1, e2; cin >> e1 >> e2; adj[e1][e2] = true;
    }
    TRAV(k, vertices) TRAV(i, vertices) TRAV(j, vertices)
        adj[i][j] |= (adj[i][k] && adj[k][j]);
    vector<pair<string, string>> result;
    REP(i, 0, SZ(vertices)) REP(j, i+1, SZ(vertices))
      if (!adj[vertices[i]][vertices[j]] && !adj[vertices[j]][vertices[i]])
        result.push_back({vertices[i], vertices[j]});
    cout << "Case " << T << ", ";
    if (SZ(result) == 0) cout << "no concurrent events.\n";
    else {
      cout << SZ(result) << " concurrent events:\n";
      if (SZ(result) > 2) result.resize(2);
      TRAV(tmp, result)
        cout << '(' << tmp.first << ',' << tmp.second << ')' << ' ';
      cout << '\n';
    }
  }
}
