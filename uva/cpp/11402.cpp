#include <bits/stdc++.h>

using namespace std;

class SegmentTree {
 private:
  size_t N; vector<size_t> seg; vector<char> lazy;
  size_t left(size_t p) { return p << 1; }
  size_t right(size_t p) { return 1 + (p << 1); }
  void build(const vector<bool> &mem, size_t p, size_t L, size_t R) {
    if (L == R) seg[p] = mem[L];
    else {
      build(mem, left(p), L, (L + R) / 2);
      build(mem, right(p), (L + R) / 2 + 1, R);
      seg[p] = seg[left(p)] + seg[right(p)];
    }
  };
  char updateCmd(char cmd) {
    switch (cmd) {
      case 'F': return 'E';
      case 'E': return 'F';
      case 'I': return 0;
      default: return 'I';
    }
  };
  void updateLazy(size_t p, size_t L, size_t R, char cmd) {
    switch(cmd) {
      case 'F':
        seg[p] = R - L + 1;
        if (L != R) lazy[left(p)] = lazy[right(p)] = cmd;
        break;
      case 'E':
        seg[p] = 0;
        if (L != R) lazy[left(p)] = lazy[right(p)] = cmd;
        break;
      case 'I':
        seg[p] = R - L + 1 - seg[p];
        if (L != R) {
          lazy[left(p)] = updateCmd(lazy[left(p)]);
          lazy[right(p)] = updateCmd(lazy[right(p)]);
        }
        break;
    }
  };
  void update(size_t p, size_t L, size_t R, size_t i, size_t j, char cmd) {
    if (lazy[p] != 0) {
      updateLazy(p, L, R, lazy[p]);
      lazy[p] = 0;
    }
    if (L > R || L > j || R < i) return;
    if (L >= i && R <= j) updateLazy(p, L, R, cmd);
    else {
      update(left(p), L, (L + R) / 2, i, j, cmd);
      update(right(p), (L + R) / 2 + 1, R, i, j, cmd);
      seg[p] = seg[left(p)] + seg[right(p)];
    }
  };
  size_t range_query(size_t p, size_t L, size_t R, size_t i, size_t j) {
    if (lazy[p] != 0) {
      updateLazy(p, L, R, lazy[p]);
      lazy[p] = 0;
    }
    if (L > R || i > R || j < L) return 0;
    if (i == L && j == R) return seg[p];
    if (j <= (L + R) / 2)
      return range_query(left(p), L, (L + R) / 2, i, j);
    if (i >= (L + R) / 2 + 1)
      return range_query(right(p), (L + R) / 2 + 1, R, i, j);
    return range_query(left(p), L, (L + R) / 2, i, (L + R) / 2)
        + range_query(right(p), (L + R) / 2 + 1, R, (L + R) / 2 + 1, j);
  };
 public:
  SegmentTree(const vector<bool> &mem) {
    N = mem.size();
    seg.assign(4 * N, 0); lazy.assign(4 * N, 0);
    build(mem, 1, 0, N - 1);
  };
  size_t range_query(size_t i, size_t j) {
    return range_query(1, 0, N - 1, i, j);
  };
  void update(size_t i, size_t j, char cmd) {
    update(1, 0, N - 1, i, j, cmd);
  };
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int tests; cin >> tests;
  for (auto test = 1; test <= tests; ++test) {
    cout << "Case " << test << ":\n";
    int M; cin >> M;
    vector<bool> mem; list<tuple<size_t, size_t, bool>> queries;
    while (M--) {
      int T; string s; cin >> T >> s;
      while (T--)
        for (auto c : s)
          mem.push_back(c == '1');
    }
    SegmentTree pirates(mem);
    int Q, query = 0; cin >> Q;
    while (Q--) {
      char cmd; size_t a, b; cin >> cmd >> a >> b;
      if (cmd == 'S')
        cout << 'Q' << ++query << ": " << pirates.range_query(a, b) << '\n';
      else pirates.update(a, b, cmd);
    }
  }
}
