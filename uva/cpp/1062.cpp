#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T = 0;
  while (++T) {
    string s; cin >> s;
    if (s == "end") break;
    vector<char> tops; char prev = 0;
    for (auto&& c : s) {
      if (c != prev) {
        auto lower = lower_bound(tops.begin(), tops.end(), c);
        if (lower == tops.end()) tops.push_back(c);
        else *lower = c;
        prev = c;
      }
    }
    cout << "Case " << T << ": " << tops.size() << '\n';
  }
}
