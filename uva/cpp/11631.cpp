#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();
int m; vector<map<int, int>> graph; vector<bool> taken; priority_queue<ii> pq;

void process(int u) {
  taken[u] = true;
  for (const auto& c : graph[u])
    if (!taken[c.first])
      pq.push({-c.second, c.first});
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int n; cin >> m >> n; if (m == 0 && n == 0) break;
    graph.clear(); graph.assign(m, {});
    int result = 0;
    while (n--) {
      int x, y, z; cin >> x >> y >> z; result += z;
      graph[x][y] = graph[y][x] = z;
    }
    taken.clear(); taken.assign(m, false);
    process(0);
    while (!pq.empty()) {
      auto tmp = pq.top(); auto w = -tmp.first, u = tmp.second; pq.pop();
      if (!taken[u]) result -= w, process(u);
    }
    cout << result << '\n';
  }
}
