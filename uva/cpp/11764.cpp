#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto i = 1; i <= T; ++i) {
    cout << "Case " << i << ": ";
    int N; cin >> N;
    int high = 0, low = 0, curr_wall, next_wall; cin >> curr_wall;
    for (auto j = 1; j < N; ++j) {
      cin >> next_wall;
      if (next_wall > curr_wall) ++high;
      else if (next_wall < curr_wall) ++low;
      curr_wall = next_wall;
    }
    cout << high << " " << low << "\n";
  }
}
