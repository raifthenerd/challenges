#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto i = 1; i <= T; ++i) {
    cout << "Case " << i << ": ";
    int N; cin >> N;
    int time, mile = 0, juice = 0;
    for (auto j = 0; j < N; ++j) {
      cin >> time;
      mile += 10 * (1 + (time / 30));
      juice += 15 * (1 + (time / 60));
    }
    if (mile < juice) cout << "Mile " << mile;
    else if (mile > juice) cout << "Juice " << juice;
    else cout << "Mile Juice " << mile;
    cout << '\n';
  }
}
