#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t SET; cin >> SET;
  while (SET--) {
    size_t N, S, Q; cin >> N >> S >> Q;
    vector<queue<size_t>> stations(N);
    for (size_t i = 0; i < N; ++i) {
      size_t qi; cin >> qi;
      while (qi--) {
        size_t dest; cin >> dest; stations[i].push(dest);
      }
    }
    stack<int> carrier;
    size_t curr = 0, result = 0;
    bool completed = true;
    for (auto&& q : stations)
      completed &= q.empty();
    while (!completed) {
      while (!carrier.empty()) {
        if (carrier.top() == curr + 1) {
          carrier.pop(); ++result;
        } else if (stations[curr].size() < Q) {
          stations[curr].push(carrier.top());
          carrier.pop(); ++result;
        } else break;
      }
      while (!stations[curr].empty() && carrier.size() < S) {
        carrier.push(stations[curr].front());
        stations[curr].pop(); ++result;
      }
      completed = carrier.empty();
      for (auto&& q : stations)
        completed &= q.empty();
      if (!completed) {
        curr = (curr + 1) % N;
        result += 2;
      }
    }
    cout << result << '\n';
  }
}
