#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int N; cin >> N;
    if (N == 0) break;
    while (true) {
      stack<int> s;
      queue<int> q;
      bool marshal = true;
      int tmp; cin >> tmp;
      if (tmp == 0) break;
      q.push(tmp);
      for (auto i = 1; i < N; ++i) {
        cin >> tmp; q.push(tmp);
      }
      for (auto i = 1; i <= N; ++i) {
        s.push(i);
        while (!s.empty() && s.top() == q.front()) {
          s.pop(); q.pop();
        }
      }
      marshal = s.empty();
      cout << (marshal ? "Yes\n" : "No\n");
    }
    cout << '\n';
  }
}
