#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    size_t K; cin >> K;
    set<char> grid[2][5]; vector<char> common[5];
    for (auto i = 0; i < 2; ++i) {
      for (auto j = 0; j < 6; ++j) {
        for (auto k = 0; k < 5; ++k) {
          char tmp; cin >> tmp; grid[i][k].insert(tmp);
        }
      }
    }
    size_t n = 1;
    for (auto i = 0; i < 5; ++i) {
      for (auto c : grid[0][i])
        if (grid[1][i].count(c) > 0)
          common[i].push_back(c);
      n *= common[i].size();
    }
    if (n < K) cout << "NO\n";
    else {
      --K; string s = "\n";
      for (auto i = 4; i >= 0; --i) {
        s = common[i][K % common[i].size()] + s;
        K /= common[i].size();
      }
      cout << s;
    }
  }
}
