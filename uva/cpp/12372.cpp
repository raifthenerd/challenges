#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, L, W, H;
  cin >> N;
  for (auto i = 1; i <= N; ++i) {
    cout << "Case " << i << ": ";
    cin >> L >> W >> H;
    if (L <= 20 && W <= 20 && H <= 20) cout << "good\n";
    else cout << "bad\n";
  }
}
