#include <bits/stdc++.h>

using namespace std;

const int NMAX = 50000;
int n_recp[NMAX], graph[NMAX], depth[NMAX];

int solve(int idx, unsigned int d) {
  if (n_recp[idx] == 0) {
    depth[idx] = d;
    if (depth[graph[idx]] > 0) {
      n_recp[idx] = d - depth[graph[idx]] + 1;
      auto key = graph[idx];
      while (key != idx) { n_recp[key] = n_recp[idx]; key = graph[key]; }
    } else {
      auto tmp = 1 + solve(graph[idx], d + 1);
      if (n_recp[idx] == 0) n_recp[idx] = tmp;
    }
    depth[idx] = 0;
  }
  return n_recp[idx];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    int N; cin >> N;
    for (auto i = 0; i < N; ++i) n_recp[i] = depth[i] = 0;
    for (auto i = 0; i < N; ++i) {
      int u, v; cin >> u >> v; graph[u-1] = v-1;
    }
    int result = 0, val = 0;
    for (auto i = 0; i < N; ++i) {
      if (val < solve(i, 1)) {
        val = solve(i, 1); result = i;
      }
    }
    cout << "Case " << t << ": " << result + 1 << '\n';
  }
}
