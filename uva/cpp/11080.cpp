#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int v, e; cin >> v >> e;
    bool guard[v], visited[v]; set<int> adj[v];
    for (auto i = 0; i < v; ++i) {
      visited[i] = false; adj[i].clear();
    }
    while (e--) {
      int f, t; cin >> f >> t; adj[f].insert(t); adj[t].insert(f);
    }
    int m = 0; queue<int> q;
    for (auto i = 0; i < v; ++i) {
      if (visited[i]) continue;
      int f = 0, t = 0;
      guard[i] = true; q.push(i); visited[i] = true;
      while (!q.empty()) {
        auto u = q.front(); q.pop(); if (guard[u]) ++t; else ++f;
        for (auto c : adj[u]) {
          if (!visited[c]) {
            guard[c] = !guard[u]; q.push(c); visited[c] = true;
          } else if (guard[c] == guard[u]) m = -1;
        }
      }
      if (m >= 0) m += f == 0 ? t : min(f, t);
    }
    cout << m << '\n';
  }
}
