#include <bits/stdc++.h>

using namespace std;

int N, x, T, K;
int price[100], favour[100], cache[100][23][1001];

int solve(int idx, int remdim, int rembud) {
  if (idx == K || remdim == 0 || rembud == 0) return 0;
  if (cache[idx][remdim][rembud] < 0) {
    auto tmp = solve(idx + 1, remdim, rembud);
    if (remdim >= 1 && rembud >= price[idx])
      tmp = max(favour[idx]+solve(idx+1, remdim-1, rembud-price[idx]),
                tmp);
    if (remdim >= 2 && rembud >= 2 * price[idx])
      tmp = max(2*favour[idx]+solve(idx+1, remdim-2, rembud-2*price[idx]),
                tmp);
    cache[idx][remdim][rembud] = tmp;
  }
  return cache[idx][remdim][rembud];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << fixed << setprecision(2);
  while (true) {
    cin >> N >> x >> T >> K;
    if (N == 0 && x == 0 && T == 0 && K == 0) break;
    ++N;
    for (auto i = 0; i < K; ++i) {
      cin >> price[i]; favour[i] = 0;
      int tmp; for (auto j = 0; j < N; ++j) { cin >> tmp; favour[i] += tmp; }
    }
    for (auto i = 0; i < 100; ++i)
      for (auto j = 0; j < 23; ++j)
        for (auto k = 0; k < 1001; ++k)
          cache[i][j][k] = -1;
    cout << static_cast<double>(solve(0, 2*N, static_cast<int>(N*x/1.1+1e-6)-N*T)) / N << '\n';
  }
}
