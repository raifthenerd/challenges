#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    vector<int> A;
    while (n--) {
      int tmp; cin >> tmp; A.push_back(tmp);
    }
    auto result = 0;
    for (auto i = 2; i <= A.size(); ++i) {
      auto a = A[i-1], b = 0;
      for (auto j = 0; j < i-1; ++j) {
        if (A[j] <= A[i-1]) ++b;
      }
      result += b;
    }
    cout << result << '\n';
  }
}
