#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int n, k; while (cin >> n) { cin >> k;
    int T[n]; REP(i, 0, n) cin >> T[i]; cin.get();
    int dist[n][100]; REP(i, 0, n) REP(j, 0, 100) dist[i][j] = oo >> 1;
    priority_queue<pair<int, ii>, vector<pair<int, ii>>,
                   greater<pair<int, ii>>> pq;
    set<int> floor[n]; REP(i, 0, n) do {
      int tmp; cin >> tmp;
      floor[i].insert(tmp); if (tmp == 0) pq.push({dist[i][0] = 0, {i, 0}});
    } while (cin.get() != '\n');
    while (!pq.empty()) {
      int i = pq.top().second.first, j = pq.top().second.second,
        w = pq.top().first; pq.pop();
      if (w > dist[i][j]) continue;
      REP(x, 0, n) if (floor[x].count(j) > 0 && dist[i][j] + 60 < dist[x][j]) {
        dist[x][j] = dist[i][j] + 60;
        pq.push({dist[x][j], {x, j}});
      }
      TRAV(y, floor[i]) if (dist[i][j] + T[i] * abs(j - y) < dist[i][y]) {
        dist[i][y] = dist[i][j] + T[i] * abs(j - y);
        pq.push({dist[i][y], {i, y}});
      }
    }
    auto result = dist[0][k]; REP(i, 1, n) result = min(result, dist[i][k]);
    if (result == oo >> 1) cout << "IMPOSSIBLE\n"; else cout << result << '\n';
  }
}
