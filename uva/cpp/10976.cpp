#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int k;
  while (cin >> k) {
    auto x = k + 1, y = k*x/(x-k), n = 0;
    list<pair<int, int>> res;
    while (y >= x) {
      if ((k*x)%(x-k) == 0) {
        y = k*x/(x-k);
        res.push_back(make_pair(y, x));
        ++n;
      }
      ++x;
    }
    cout << n << '\n';
    for (auto&& c : res)
      cout << "1/" << k << " = 1/" << c.first << " + 1/" << c.second << '\n';
  }
}
