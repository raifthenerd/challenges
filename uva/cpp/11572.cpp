#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    map<int, int> package; int result = 0, prev = 0, curr = 0;
    while (n--) {
      int snow; cin >> snow;
      if (package.count(snow) > 0 && package[snow] >= prev) {
        result = max(result, curr - prev);
        prev = package[snow] + 1;
      }
      package[snow] = curr++;
    }
    result = max(result, curr - prev);
    cout << result << '\n';
  }
}
