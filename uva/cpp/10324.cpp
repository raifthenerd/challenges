#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T = 0;
  while (++T) {
    string bin; if (!(cin >> bin)) break;
    cout << "Case " << T << ":\n";
    int n; cin >> n;
    while (n--) {
      bool yes = true;
      int i, j; cin >> i >> j;
      if (i > j) swap(i, j);
      auto c = bin[i];
      for (auto k = i+1; k <= j; ++k) {
        if (c != bin[k]) {
          yes = false; break;
        }
      }
      cout << (yes ? "Yes" : "No") << '\n';
    }
  }
}
