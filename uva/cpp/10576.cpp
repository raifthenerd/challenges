#include <bits/stdc++.h>

using namespace std;

long long s, d;
vector<long long> postings;

bool check() {
  for (auto i = 0; i < 8; ++i) {
    long long tmp = 0;
    for (auto j = 0; j < 5; ++j) tmp += postings[i+j];
    if (tmp > 0) return false;
  }
  return true;
}

long long get_total() {
  long long result = 0;
  for (auto c : postings) result += c;
  return result;
}

long long bt(long long curr, int idx) {
  if (idx >= 12) return curr;
  for (auto c : {s, d}) {
    if (check()) curr = bt(max(curr, get_total()), idx + 1);
    postings[idx] = c;
  }
  return curr;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (cin >> s) {
    cin >> d; d *= -1;
    long long surplus = 12 * d;
    postings.clear(); postings.assign(12, d);
    auto result = bt(surplus, 0);
    if (result < 0) cout << "Deficit\n";
    else cout << result << '\n';
  }
}
