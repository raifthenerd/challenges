#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << "Lumberjacks:\n";
  int a[10], N; cin >> N;
  for (auto i = 0; i < N; ++i) {
    for (auto j = 0; j < 10; ++j) cin >> a[j];
    bool ordered = true;
    int tmp = a[1] - a[0];
    for (auto j = 1; j < 9; ++j) {
      if (tmp * (a[j+1] - a[j]) < 0) {
        ordered = false; break;
      }
    }
    cout << (ordered ? "Ordered" : "Unordered") << "\n";
  }
}
