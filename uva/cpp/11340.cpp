#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << setfill('0');
  int N; cin >> N;
  while (N--) {
    int K; cin >> K;
    map<char, uint64_t> values;
    while (K--) {
      char c; uint64_t v; cin >> c >> v;
      values[c] = v;
    }
    int M; cin >> M; cin.ignore(1, '\n');
    uint64_t price = 0;
    while (M--) {
      string s; getline(cin, s);
      for (auto&& c : s) {
        auto it = values.find(c);
        if (it != values.end()) price += it->second;
      }
    }
    cout << (price / 100) << '.' << setw(2) << (price % 100) << "$\n";
  }
}
