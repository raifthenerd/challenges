#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int n, m, tmp; cin >> n >> m; if (n == 0 && m == 0) break;
    vector<int> dragon, knight;
    while (n--) {
      cin >> tmp; dragon.push_back(tmp);
    }
    while (m--) {
      cin >> tmp; knight.push_back(tmp);
    }
    sort(dragon.begin(), dragon.end());
    sort(knight.begin(), knight.end());
    auto it = knight.begin();
    size_t result = 0; bool slay = true;
    for (auto d : dragon) {
      it = lower_bound(it, knight.end(), d);
      if (it == knight.end()) {
        slay = false; break;
      }
      result += *it; ++it;
    }
    if (slay) cout << result << '\n';
    else cout << "Loowater is doomed!\n";
  }
}
