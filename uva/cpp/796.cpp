#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

vector<vi> graph;
vi dfs_num, dfs_low, dfs_parent;
vii results;
int counter;

void bridge(int node) {
  dfs_num[node] = dfs_low[node] = ++counter;
  for (auto c : graph[node]) {
    if (dfs_num[c] == oo) {
      dfs_parent[c] = node;
      bridge(c);
      if (dfs_low[c] > dfs_num[node])
        results.push_back({min(c, node), max(c, node)});
      dfs_low[node] = min(dfs_low[node], dfs_low[c]);
    } else if (c != dfs_parent[node])
      dfs_low[node] = min(dfs_low[node], dfs_num[c]);
  }
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int N;
  while (cin >> N) {
    graph.clear(); graph.assign(N, {});
    for (auto i = 0; i < N; ++i) {
      char tmp;
      int node; cin >> node >> tmp;
      int m; cin >> m >> tmp;
      while (m--) {
        int tmp; cin >> tmp;
        graph[node].push_back(tmp); graph[tmp].push_back(node);
      }
    }
    dfs_num.clear(); dfs_num.assign(N, oo);
    dfs_low.clear(); dfs_low.assign(N, 0);
    dfs_parent.clear(); dfs_parent.assign(N, 0);
    results.clear(); counter = 0;
    for (auto i = 0; i < N; ++i) if (dfs_num[i] == oo) bridge(i);
    cout << results.size() << " critical links\n";
    sort(ALL(results));
    for (const auto& c : results) cout << c.first << " - " << c.second << '\n';
    cout << '\n';
  }
}
