#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N;
  string s;
  cin >> N;
  for (auto i = 0; i < N; ++i) {
    cin >> s;
    if (s.length() == 5) cout << "3\n";
    else if (s[0] == 'o' && s[1] == 'n') cout << "1\n";
    else if (s[0] == 'o' && s[2] == 'e') cout << "1\n";
    else if (s[1] == 'n' && s[2] == 'e') cout << "1\n";
    else cout << "2\n";
  }
}
