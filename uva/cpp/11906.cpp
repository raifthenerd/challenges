#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    cout << "Case " << t << ": ";
    int R, C, M, N, W; cin >> R >> C >> M >> N >> W;
    int water[R][C], visited[R][C];
    for (auto x = 0; x < R; ++x)
      for (auto y = 0; y < C; ++y)
        water[x][y] = visited[x][y] = false;
    while (W--) { int x, y; cin >> x >> y; water[x][y] = true; }

    queue<pair<int, int>> q; q.push({0, 0}); visited[0][0] = true;
    int odd = 0, even = 0;
    while (!q.empty()) {
      auto curr = q.front(); q.pop();
      int k = 0;
      for (auto dx : {-1, 1}) {
        for (auto dy : {-1, 1}) {
          for (auto i = 0; i < 2; ++i) {
            auto x = curr.first+M*dx, y = curr.second+N*dy;
            if (0 <= x && x < R && 0 <= y && y < C && !water[x][y]) {
              ++k; if (!visited[x][y]) {
                q.push({x, y}); visited[x][y] = true;
              }
            }
            swap(M, N);
          }
        }
      }
      if (M*N == 0 || M == N) k /= 2;
      if (k % 2 == 0) ++even; else ++odd;
    }
    cout << even << ' ' << odd << '\n';
  }
}
