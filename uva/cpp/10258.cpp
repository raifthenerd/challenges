#include <bits/stdc++.h>

using namespace std;

struct Contestant { int num, solved, penalty; };
struct ContestantComp {
  bool operator()(const Contestant& lhs, const Contestant& rhs) const {
    if (lhs.solved == rhs.solved) {
      if (lhs.penalty == rhs.penalty) {
        return lhs.num < rhs.num;
      } return lhs.penalty < rhs.penalty;
    } return lhs.solved > rhs.solved;
  }
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int cases; cin >> cases; cin.get();
  while (cases--) {
    cin.get();
    map<pair<int, int>, int> not_solved;
    map<int, pair<int, int>> scores;
    set<Contestant, ContestantComp> result;
    int c, p, t; char L;
    while (cin.peek() != '\n' && !cin.eof()) {
      cin >> c >> p >> t >> L; cin.get();
      if (scores.find(c) == scores.end()) scores[c] = make_pair(0, 0);
      auto tmp = make_pair(c, p);
      if (L == 'C') {
        if (not_solved[tmp] >= 0) {
          ++scores[c].first;
          scores[c].second += t + 20 * not_solved[tmp];
          not_solved[tmp] = -1;
        }
      } else if (L == 'I' && not_solved[tmp] >= 0) ++not_solved[tmp];
    }
    for (auto&& tmp : scores)
      result.insert({tmp.first, tmp.second.first, tmp.second.second});
    for (auto&& tmp : result)
      cout << tmp.num << ' ' << tmp.solved << ' ' << tmp.penalty << '\n';
    if (cases) cout << '\n';
  }
}
