#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int t; cin >> t;
  while (t--) {
    int n, p; cin >> n >> p;
    vector<int> xs;
    while (p--) {
      int x; cin >> x; xs.push_back(x);
    }
    bool result = false;
    for (size_t i = 0; i < (1U << xs.size()); ++i) {
      auto tmp = 0;
      for (size_t j = 0; j < xs.size(); ++j) {
        if (i & (1 << j)) tmp += xs[j];
      }
      if (n == tmp) {
        result = true;
        break;
      }
    }
    cout << (result ? "YES" : "NO") << '\n';
  }
}
