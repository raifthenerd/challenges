#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  vector<int> tmp (5, 0);
  while (true) {
    int n; cin >> n; if (n == 0) break;
    map<unsigned long long, int> mem; int result = 0, popularity = 0;
    while (n--) {
      unsigned long long key = 0, digit = 1;
      for (auto i = 0; i < 5; ++i) cin >> tmp[i];
      sort(tmp.begin(), tmp.end());
      for (auto t : tmp) {
        key += digit * (t - 100);
        digit *= 400;
      }
      if (mem.count(key) == 0) mem[key] = 0;
      ++mem[key];
      if (mem[key] > popularity) popularity = mem[key];
    }
    for (auto&& m : mem)
      if (popularity == m.second)
        ++result;
    result *= popularity;
    cout << result << '\n';
  }
}
