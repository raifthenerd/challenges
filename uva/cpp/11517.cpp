#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int p, n; cin >> p >> n;
    int coins[n], mem[20001];
    while (n--) cin >> coins[n];
    for (auto i = 0; i <= 20000; ++i) mem[i] = 20001; mem[0] = 0;
    for (auto c : coins) {
      for (auto i = 20000; i >= 0; --i) {
        if (i >= c) mem[i] = min(mem[i], 1 + mem[i-c]);
      }
    }
    for (auto i = p; i <= 20000; ++i) {
      if (mem[i] < 20001) {
        cout << i << ' ' << mem[i] << '\n';
        break;
      }
    }
  }
}
