#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int t;
  cin >> t;
  for (auto i = 0; i < t; ++i) {
    int n, tmp, max = 0, min = 99;
    cin >> n;
    for (auto j = 0; j < n; ++j) {
      cin >> tmp;
      if (max < tmp) max = tmp;
      if (min > tmp) min = tmp;
    }
    cout << 2 * (max - min) << "\n";
  }
}
