#include <bits/stdc++.h>

using namespace std;

vector<vector<int>> adj; vector<bool> visited, color;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int n, l; cin >> n; if (n == 0) break; cin >> l;
    adj.clear(); adj.assign(n, {});
    while (l--) {
      int a, b; cin >> a >> b; adj[a].push_back(b); adj[b].push_back(a);
    }
    visited.clear(); visited.assign(n, false);
    color.clear(); color.assign(n, false);
    bool bicolor = true; queue<int> q;
    for (auto i = 0; i < n && bicolor; ++i) {
      if (visited[i]) continue;
      q.push(i); visited[i] = true;
      while (!q.empty() && bicolor) {
        auto j = q.front(); q.pop();
        for (auto k : adj[j]) {
          if (!visited[k]) {
            q.push(k); visited[k] = true; color[k] = !color[j];
          } else if (color[k] == color[j]) {
            bicolor = false; break;
          }
        }
      }
    }
    if (!bicolor) cout << "NOT "; cout << "BICOLORABLE.\n";
  }
}
