#include <bits/stdc++.h>

using namespace std;

const int T = 1000000;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    unsigned int n, m; cin >> n >> m;
    if (n == 0 && m == 0) break;
    bool schedule[T] = {false};
    bool conflict = false;
    unsigned int st, et, itv;
    while (n--) {
      cin >> st >> et;
      if (!conflict) {
        for (auto i = st; i < et; ++i) {
          if (schedule[i]) conflict = true;
          else schedule[i] = true;
        }
      }
    }
    while (m--) {
      cin >> st >> et >> itv;
      if (!conflict) {
        while (st < T) {
          if (et >= T) et = T;
          for (auto i = st; i < et; ++i) {
            if (schedule[i]) conflict = true;
            else schedule[i] = true;
          }
          if (conflict) break;
          st += itv; et += itv;
        }
      }
    }
    cout << (conflict ? "CONFLICT\n" : "NO CONFLICT\n");
  }
}
