#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    size_t n; cin >> n;
    vector<size_t> ws(n), incr(n), decr(n);
    for (size_t i = 0; i < n; ++i) cin >> ws[n-1-i];
    for (size_t i = 0; i < n; ++i) {
      for (size_t j = 0; j < i; ++j) {
        if (ws[j] < ws[i] && incr[j] > incr[i]) incr[i] = incr[j];
        else if (ws[j] > ws[i] && decr[j] > decr[i]) decr[i] = decr[j];
      }
      ++incr[i]; ++decr[i];
    }
    size_t result = 0;
    for (size_t i = 0; i < n; ++i) result = max(result, incr[i] + decr[i] - 1);
    cout << result << '\n';
  }
}
