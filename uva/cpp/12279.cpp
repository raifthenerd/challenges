#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, i = 0;
  while (++i) {
    cin >> N;
    if (N == 0) break;
    int tmp, result = 0;
    cout << "Case " << i << ": ";
    for (auto j = 0; j < N; ++j) {
      cin >> tmp;
      result += tmp > 0 ? 1 : -1;
    }
    cout << result << "\n";
  }
}
