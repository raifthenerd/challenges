#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

vector<vi> graph;
vi dfs_num, dfs_low, dfs_parent, pigeons;
int counter, root, rootChild;

void articulationPoint(int node) {
  dfs_num[node] = dfs_low[node] = ++counter;
  for (auto c : graph[node]) {
    if (dfs_num[c] == oo) {
      dfs_parent[c] = node; ++pigeons[c];
      if (node == root) ++rootChild;
      articulationPoint(c);
      if (dfs_low[c] >= dfs_num[node]) ++pigeons[node];
      dfs_low[node] = min(dfs_low[node], dfs_low[c]);
    } else if (c != dfs_parent[node])
      dfs_low[node] = min(dfs_low[node], dfs_num[c]);
  }
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int n, m; cin >> n >> m; if (n == 0 && m == 0) break;
    graph.clear(); graph.assign(n, {});
    while (true) {
      int x, y; cin >> x >> y; if (x == -1 && y == -1) break;
      graph[x].push_back(y); graph[y].push_back(x);
    }
    dfs_num.clear(); dfs_num.assign(n, oo);
    dfs_low.clear(); dfs_low.assign(n, 0);
    dfs_parent.clear(); dfs_parent.assign(n, 0);
    pigeons.clear(); pigeons.assign(n, 0);
    counter = 0;
    for (auto i = 0; i < n; ++i) {
      if (dfs_num[i] == oo) {
        root = i; rootChild = 0; articulationPoint(root);
        pigeons[root] = rootChild;
      }
    }
    vii result; result.clear();
    for (auto i = 0; i < n; ++i) result.push_back({i, pigeons[i]});
    sort(result.begin(), result.end(), [](const ii& lhs, const ii& rhs) {
                        if (lhs.second == rhs.second)
                          return lhs.first < rhs.first;
                        return lhs.second > rhs.second;
                      });
    for (auto i = 0; i < m; ++i)
      cout << result[i].first << ' ' << result[i].second << '\n';
    cout << '\n';
  }
}
