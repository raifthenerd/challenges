#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int D; cin >> D;
    list<tuple<string, int, int>> db;
    while (D--) {
      string M; int L, H; cin >> M >> L >> H;
      db.push_back(make_tuple(M, L, H));
    }
    int Q; cin >> Q;
    while (Q--) {
      int P; cin >> P;
      bool found = false; string res;
      for (auto&& item : db) {
        if (get<1>(item) <= P && P <= get<2>(item)) {
          if (!found) {
            res = get<0>(item);
            found = true;
          } else {
            res = "UNDETERMINED";
            break;
          }
        }
      }
      if (!found) res = "UNDETERMINED";
      cout << res << '\n';
    }
    if (T) cout << '\n';
  }
}
