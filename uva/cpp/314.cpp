#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();
const vii diffs = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
const map<string, int> direction = {{"east", 0}, {"south", 1},
                                    {"west", 2}, {"north", 3}};

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int M, N; cin >> M >> N; if (M == 0 && N == 0) break;
    bool grid[M+1][N+1]; int dist[M+1][N+1][4];
    REP(i, 0, M+1) REP(j, 0, N+1) {
      grid[i][j] = false; REP(k, 0, 4) dist[i][j][k] = -1;
    }
    REP(i, 0, M) REP(j, 0, N) {
      bool tmp; cin >> tmp;
      if (tmp) grid[i][j] = grid[i][j+1] = grid[i+1][j] = grid[i+1][j+1] = true;
    }
    ii B, E; string tmp;
    cin >> B.first >> B.second >> E.first >> E.second >> tmp;
    if (B == E) { cout << "0\n"; continue; }
    auto dir = direction.at(tmp); auto found = false;
    queue<pair<ii, int>> q; q.push({B, dir}); dist[B.first][B.second][dir] = 0;
    while (!found && !q.empty()) {
      auto x = q.front().first.first, y = q.front().first.second,
        dir = q.front().second; q.pop();
      auto d = dist[x][y][dir] + 1;
      for (const auto& diff : {1, 3}) {
        if (dist[x][y][(dir+diff)%4] < 0)
          q.push({{x, y}, (dir+diff)%4}), dist[x][y][(dir+diff)%4] = d;
      }
      for (auto n = 1; n <= 3; ++n) {
        auto tmpx = x + n * diffs[dir].first, tmpy = y + n * diffs[dir].second;
        if (tmpx >= M || tmpx <= 0 || tmpy >= N || tmpy <= 0 || grid[tmpx][tmpy])
          break;
        if (dist[tmpx][tmpy][dir] < 0) {
          q.push({{tmpx, tmpy}, dir}), dist[tmpx][tmpy][dir] = d;
          if (tmpx == E.first && tmpy == E.second) {
            found = true; cout << d << '\n'; break;
          }
        }
      }
    }
    if (!found) cout << "-1\n";
  }
}
