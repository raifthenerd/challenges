#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int M;
  while (cin >> M) {
    vector<pair<int, int>> inits, targets;
    for (auto i = 0; i < M; ++i) {
      for (auto j = 0; j < M; ++j) {
        char x; cin >> x;
        if (x == '1') inits.push_back({i, j});
        else if (x == '3') targets.push_back({i, j});
      }
    }
    auto result = 0;
    for (auto&& i : inits) {
      auto steps = 2 * (M - 1);
      for (auto&& t : targets) {
        auto tmp = abs(i.first - t.first) + abs(i.second - t.second);
        steps = min(steps, tmp);
      }
      result = max(result, steps);
    }
    cout << result << '\n';
  }
}
