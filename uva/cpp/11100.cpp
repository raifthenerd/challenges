#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  bool first = true;
  while (true) {
    int n; cin >> n; if (n == 0) break;
    if (first) first = false;
    else cout << '\n';
    vector<int> bags; map<int, int> count; int k = 0;
    while (n--) {
      int x; cin >> x; bags.push_back(x);
      if (k < ++count[x]) k = count[x];
    }
    cout << k << '\n';
    sort(bags.begin(), bags.end());
    vector<vector<int>> results(k); int idx = 0;
    for (auto it = bags.begin(); it != bags.end(); ++it) {
      results[idx].push_back(*it);
      idx = (idx + 1) % k;
    }
    for (auto&& res : results) {
      for (auto it = res.begin(); it != res.end(); ++it) {
        if (it != res.begin()) cout << ' '; cout << *it;
      }
      cout << '\n';
    }
  }
}
