#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int n; vector<map<int, int>> graph; vector<bool> taken;
priority_queue<ii> pq;

void process(int u) {
  taken[u] = true;
  for (const auto& c : graph[u]) {
    if (!taken[c.first]) pq.push({-c.second, c.first});
  }
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int m; cin >> n >> m; if (n == 0 && m == 0) break;
    graph.clear(); graph.assign(n, {});
    while (m--) {
      int u, v, w; cin >> u >> v >> w; graph[u][v] = graph[v][u] = w;
    }
    taken.clear(); taken.assign(n, false);
    vi result; result.clear();
    for (auto i = 0; i < n; ++i) {
      if (taken[i]) continue;
      process(i);
      while (!pq.empty()) {
        auto tmp = pq.top(); pq.pop();
        auto u = tmp.second, w = -tmp.first;
        if (!taken[u]) process(u); else result.push_back(w);
      }
    }
    if (result.empty()) {
      cout << "forest\n"; continue;
    }
    sort(ALL(result));
    auto first = true;
    for (auto c : result) {
      if (first) first = false; else cout << ' '; cout << c;
    }
    cout << '\n';
  }
}
