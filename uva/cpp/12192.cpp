#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    size_t N, M, Q; cin >> N >> M; if (N == 0 && M == 0) break;
    vector<vector<int>> H(N + M - 1);
    for (size_t i = 0; i < N; ++i) {
      for (size_t j = 0; j < M; ++j) {
        int tmp; cin >> tmp; H[N-1-i+j].push_back(tmp);
      }
    }
    cin >> Q; while (Q--) {
      int L, U, res = 0; cin >> L >> U;
      for (auto&& tmp : H) {
        auto lit = lower_bound(tmp.begin(), tmp.end(), L),
             uit = upper_bound(tmp.begin(), tmp.end(), U);
        res = max(res, static_cast<int>(distance(lit, uit)));
      }
      cout << res << '\n';
    }
    cout << "-\n";
  }
}
