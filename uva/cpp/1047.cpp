#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int cases = 0;
  while (true) {
    size_t n, b; cin >> n >> b; if (n == 0 && b == 0) break;
    cout << "Case Number  " << ++cases << '\n';
    vector<int> customers; map<int, int> common;
    while (n--) {
      int c; cin >> c; customers.push_back(c);
    }
    size_t m; cin >> m;
    while (m--) {
      int t; cin >> t;
      int idx = 0;
      while (t--) {
        int x; cin >> x; idx |= (1 << (x - 1));
      }
      int c; cin >> c; common[idx] += c;
      for (size_t i = 0; i < customers.size(); ++i)
        if (idx & (1 << i))
          customers[i] -= c;
    }
    int n_result = 0; set<int> result;
    vector<bool> selected(customers.size(), false);
    for (size_t i = 0; i < b; ++i) selected[i] = true;
    do {
      int idx = 0, tmp_result = 0; vector<int> tmp_selected;
      for (size_t i = 0; i < customers.size(); ++i) {
        if (selected[i]) {
          idx |= 1 << i;
          tmp_result += customers[i];
          tmp_selected.push_back(i);
        }
      }
      for (auto c : common)
        if (idx & c.first)
          tmp_result += c.second;
      if (n_result < tmp_result) {
        n_result = tmp_result;
        result.clear(); for (auto c : tmp_selected) result.insert(c+1);
      }
    } while (prev_permutation(selected.begin(), selected.end()));
    cout << "Number of Customers: " << n_result << '\n';
    cout << "Locations recommended:";
    for (auto c : result) cout << ' ' << c;
    cout << "\n\n";
  }
}
