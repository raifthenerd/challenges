#include <bits/stdc++.h>

using namespace std;

int roman_to_num(string& roman) {
  int result = 0;
  for (auto it = roman.cbegin(); it != roman.cend(); ++it) {
    auto tmp = next(it);
    switch (*it) {
      case 'I':
        if (tmp == roman.cend()) result += 1;
        else if (*tmp == 'V') {
          result += 4; ++it;
        } else if (*tmp == 'X') {
          result += 9; ++it;
        } else result += 1;
        break;
      case 'V':
        result += 5;
        break;
      case 'X':
        if (tmp == roman.cend()) result += 10;
        else if (*tmp == 'L') {
          result += 40; ++it;
        } else if (*tmp == 'C') {
          result += 90; ++it;
        } else result += 10;
        break;
      case 'L':
        result += 50;
        break;
      case 'C':
        if (tmp == roman.cend()) result += 100;
        else if (*tmp == 'D') {
          result += 400; ++it;
        } else if (*tmp == 'M') {
          result += 900; ++it;
        } else result += 100;
        break;
      case 'D':
        result += 500;
        break;
      case 'M':
        result += 1000;
        break;
    }
  }
  return result;
}

const int TABLE[10] = {0, 1, 2, 3, 2, 1, 2, 3, 4, 2};
int num_to_roman_len(int n) {
  int result = n/1000; n %= 1000;
  for (auto i = 0; i < 3; ++i) {
    result += TABLE[n%10]; n /= 10;
  }
  return result;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  string roman; int result = 0;
  while (cin >> roman)
    result += roman.size() - num_to_roman_len(roman_to_num(roman));
  cout << result << endl;
}
