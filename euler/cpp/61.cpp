#include <bits/stdc++.h>

using namespace std;

void check_and_push(map<int, set<int>>& s, int n) {
  if (n < 10000 && n > 999 && n % 100 > 9)
    s[n / 100].insert(n % 100);
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  map<int, set<int>> tri, sq, penta, hexa, hepta, octa;
  for (auto n = 1; n < 200; ++n) {
    check_and_push(tri, n*(n+1)/2);
    check_and_push(sq, n*n);
    check_and_push(penta, n*(3*n-1)/2);
    check_and_push(hexa, n*(2*n-1));
    check_and_push(hepta, n*(5*n-3)/2);
    check_and_push(octa, n*(3*n-2));
  }
  vector<map<int, set<int>>> Ps = {sq, penta, hexa, hepta, octa};
  sort(Ps.begin(), Ps.end());
  do {
    for (auto& tmp : tri) {
      auto k1 = tmp.first;
      for (auto& k2 : tmp.second) {
        for (auto& k3 : Ps[0][k2]) {
          for (auto& k4 : Ps[1][k3]) {
            for (auto& k5 : Ps[2][k4]) {
              for (auto& k6 : Ps[3][k5]) {
                for (auto& k7 : Ps[4][k6]) {
                  if (k1 == k7)
                    cout << 101 * (k1 + k2 + k3 + k4 + k5 + k6) << endl;
                }
              }
            }
          }
        }
      }
    }
  } while (next_permutation(Ps.begin(), Ps.end()));
}
