#include <iostream>
#include <string>
#include <algorithm>

int main() {
  std::ios::sync_with_stdio(false);
  const std::string digits = "123456789";
  uint i = 1, base = 10, result = 918273645;
  auto db = digits.cbegin(), de = digits.cend();
  while (i < 1000) {
    uint tmp = 2, n = 9*base + i++;
    auto s = std::to_string(n);
    while (s.size() < 9) s += std::to_string(n * tmp++);
    if (std::is_permutation(db, de, s.begin())) {
      tmp = std::stoul(s);
      if (result < tmp) result = tmp;
    }
    if (i >= base) base *= 10;
  }
  std::cout << result << std::endl;
}
