#include <bits/stdc++.h>

using namespace std;

double m(pair<double, double>& location) {
  return -4.0 * location.first / location.second;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n = 0;
  pair<double, double> prev = {0.0, 10.1}, curr = {1.4, -9.6};
  auto dir = make_pair(curr.first - prev.first, curr.second - prev.second);
  while (++n) {
    auto theta = atan2(curr.second, 4.0 * curr.first);
    auto norm = make_pair(cos(theta), sin(theta));
    auto tmp = 2.0 * (norm.first * dir.first + norm.second * dir.second);
    dir = make_pair(dir.first - tmp * norm.first,
                    dir.second - tmp * norm.second);
    auto alpha = - (8.0 * dir.first * curr.first
                    + 2.0 * dir.second * curr.second) /
                 (4.0 * dir.first * dir.first + dir.second * dir.second);
    curr.first += alpha * dir.first; curr.second += alpha * dir.second;
    if (curr.second > 0.0 && curr.first >= -0.01 && curr.first <= 0.01)
      break;
    prev = curr;
  }
  cout << n << endl;
}
