#include <iostream>

int main() {
  std::ios::sync_with_stdio(false);

  char result[160] = {};
  result[158] = 1;
  for (auto i = 2; i < 100; ++i) {
    char carry = 0;
    for (auto j = 158; j >= 0 || carry; --j) {
      auto tmp = i*result[j]+carry;
      result[j] = tmp%10;
      carry = tmp/10;
    }
  }
  int sum = 0;
  for (auto i = 0; i < 160; ++i) sum += result[i];
  std::cout << sum << std::endl;
}
