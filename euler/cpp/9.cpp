#include <iostream>

int main() {
  std::ios::sync_with_stdio(false);
  for (auto b = 251; b < 500; ++b) {
    for (auto a = b; a > 500-b; --a) {
      if (1000*(a+b)-a*b == 500000) {
        std::cout << a*b*(1000-a-b) << std::endl;
      }
    }
  }
}
