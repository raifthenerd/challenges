#include <bits/stdc++.h>

using namespace std;

const int FACTOS[10] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
int f(int n) {
  auto result = 0;
  while (n > 0) {
    result += FACTOS[n%10];
    n /= 10;
  }
  return result;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  auto result = 0;
  for (auto n = 1; n <= 1000000; ++n) {
    set<int> chain;
    auto i = n;
    do {
      chain.insert(i);
      i = f(i);
    } while (chain.count(i) == 0);
    if (chain.size() == 60) ++result;
  }
  cout << result << endl;
}
