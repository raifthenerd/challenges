#include <iostream>
#include <algorithm>
int main() {
  std::ios::sync_with_stdio(false);
  uint digits[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  uint primes[] = {2, 3, 5, 7, 11, 13, 17};
  uint64_t result = 0;
  do {
    bool subdivisible = true;
    for (auto i = 1; i < 8; ++i) {
      if ((100*digits[i]+10*digits[i+1]+digits[i+2]) % primes[i-1] != 0) {
        subdivisible = false;
        break;
      }
    }
    if (subdivisible) {
      uint base = 1000000000;
      for (auto i = 0; i < 10; ++i) {
        result += base*digits[i];
        base /= 10;
      }
    }
  } while (std::next_permutation(digits, digits+10));

  std::cout << result << std::endl;
}
