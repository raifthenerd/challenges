#include <iostream>

const char MOD[] = {3, 0, 3, 2, 3, 2, 3, 3, 2, 3, 2, 3};

int main() {
  std::ios::sync_with_stdio(false);
  int init = 1, result = 0;
  for (auto year = 1900; year <= 2000; ++year) {
    bool is_leap = !(year%400);
    if ((year%100) && !(year%4)) is_leap = true;
    for (auto month = 0; month < 12; ++month) {
      init += MOD[month];
      if (is_leap && month == 1) ++init;
      init = init % 7;
      if (year > 1900 && !init) ++result;
    }
  }
  std::cout << result << std::endl;
}
