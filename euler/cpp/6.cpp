#include <iostream>

int main() {
  std::ios::sync_with_stdio(false);
  auto foo = 1, bar = 1;
  for (auto i = 2; i <= 100; ++i) {
    foo += i;
    bar += i*i;
  }
  std::cout << (foo * foo - bar) << std::endl;
}
