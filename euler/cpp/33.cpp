#include <iostream>

int gcd(int a, int b) {
  if (a > b) std::swap(a, b);
  if (a == 0) return b;
  return gcd(b%a, a);
}

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  int numerator = 1, denominator = 1;
  for (int a = 1; a < 10; ++a) {
    for (int b = 1; b < 10; ++b) {
      for (int i = 1; i < 10; ++i) {
        if (a == b && b == i) break;
        int n = 10*a+i, d = 10*i+b;
        if (9*a*b == i*(10*a-b)) {
          if (n > d) std::swap(n, d);
          numerator *= n; denominator *= d;
        }
      }
    }
  }
  std::cout << denominator / gcd(numerator, denominator) << std::endl;
}
