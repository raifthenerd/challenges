#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long numerator = 0, denominator = 1;
  for (auto n = 2; n <= 16; ++n) denominator *= n;
  for (auto n = (1 << 8) - 1; n < (1 << 15) - 1; ++n) {
    unsigned long long tmp = 1;
    auto m = n, b = 0;
    for (auto idx = 1; idx <= 15; ++idx) {
      if (!(m & 1)) tmp *= idx;
      else ++b;
      m >>= 1;
    }
    if (b >= 8) numerator += tmp;
  }
  cout << denominator / numerator << endl;
}
