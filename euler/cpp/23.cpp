#include <iostream>
#include <list>
#include <set>

std::list<uint> primes = {2, 3};
std::list<uint> abundants = {12};

void gen_prime_until(uint n) {
  uint m = primes.back()+2;
  while (m < n) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

bool is_abundant(uint n) {
  if (n == 1) return false;
  gen_prime_until(n);
  uint64_t result = 1;
  uint m = n;
  for (auto p : primes) {
    if (p > n) break;
    uint tmp = 1;
    while (!(m%p)) {
      m /= p;
      tmp *= p;
    }
    if (tmp > 1) result *= (tmp*p-1)/(p-1);
  }
  return result > (n << 1);
}

void gen_abundant_until(uint n) {
  uint m = abundants.back()+1;
  while (m < n) {
    if (is_abundant(m)) abundants.push_back(m);
    ++m;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  gen_abundant_until(28123);
  std::set<uint> tmp;
  for (auto i = 1; i <= 28123; ++i) tmp.insert(i);
  for (auto i = abundants.cbegin(); i != abundants.cend(); ++i) {
    for (auto j = i; j != abundants.cend();) {
      auto val = *i+*j++;
      if (val > 28123) break;
      tmp.erase(val);
    }
  }
  uint64_t sum = 0;
  for (auto i : tmp) sum += i;
  std::cout << sum << std::endl;
}
