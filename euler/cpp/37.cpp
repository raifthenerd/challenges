#include <iostream>
#include <list>
#include <algorithm>

std::list<uint> primes = {2, 3, 5, 7};

bool trunc(uint n) {
  uint m = n/10, tmp = 10;
  auto pb = primes.begin(), pe = primes.end();
  while (m > 0 || tmp <= n) {
    if (!(std::binary_search(pb, pe, m) &&
          std::binary_search(pb, pe, n % tmp)))
      return false;
    tmp *= 10;
    m /= 10;
  }
  return true;
}

int main() {
  std::ios::sync_with_stdio(false);
  uint sum = 0, i = 0, m = 11;
  while (i < 11) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) {
      primes.push_back(m);
      if (trunc(m)) {
        sum += m;
        ++i;
      }
    }
    m += 2;
  }
  std::cout << sum << std::endl;
}
