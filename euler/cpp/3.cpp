#include <iostream>
#include <cmath>
#include <list>


const int64_t N = 600851475143;

int main() {
  std::ios::sync_with_stdio(false);
  std::list<int> primes = {};
  int result = 1;
  for (auto m = 2; m <= static_cast<int>(sqrt(N)); ++m) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) {
      primes.push_back(m);
      if (!(N%m)) result = m;
    }
  }
  std::cout << result << std::endl;
}
