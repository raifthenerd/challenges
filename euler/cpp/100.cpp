#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long n = 21, b = 15;
  const unsigned long long N = 1000000000000;
  while (n <= N) {
    auto tmp = b;
    b = 3 * tmp + 2 * n - 2;
    n = 4 * tmp + 3 * n - 3;
  }
  cout << b << endl;
}
