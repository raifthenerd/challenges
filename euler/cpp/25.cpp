#include <iostream>
#include <cmath>

int main() {
  std::ios::sync_with_stdio(false);
  int i = 1+static_cast<int>(
      (999+std::log10(std::sqrt(5)))/std::log10(.5*(1+std::sqrt(5))));
  std::cout << i << std::endl;
}
