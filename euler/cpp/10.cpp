#include <iostream>
#include <list>


int main() {
  std::ios::sync_with_stdio(false);
  std::list<int> primes = {2, 3};
  int64_t sum = 5;
  int n = 5;
  while (true) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(n%p)) {
        is_prime = false;
        break;
      }
      if (p*p > n) break;
    }
    if (is_prime) {
      primes.push_back(n);
      sum += n;
    }
    n += 2;
    if (n >= 2000000) break;
  }
  std::cout << sum << std::endl;
}
