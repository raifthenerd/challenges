#include <iostream>

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  const uint64_t facs[] = {1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880};
  uint64_t result;
  for (uint64_t n = 3; n < 2540160; ++n) {
    auto tmp = n, val = n;
    while (tmp >= 10) {
      val -= facs[tmp % 10];
      tmp /= 10;
    }
    val -= facs[tmp];
    if (!val) result += n;
  }
  std::cout << result << std::endl;
}
