#include <iostream>

int min(int a, int b, int c) {
  return std::min(a, std::min(b, c));
}

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  int base = 40755;
  int tri = base, penta = base, hexa = base;
  int tri_idx = 285, penta_idx = 165, hexa_idx = 143;
  do {
    auto tmp = min(tri, penta, hexa);
    if (tmp == tri) tri += 1 + tri_idx++;
    else if (tmp == penta) penta += 1 + 3 * penta_idx++;
    else if (tmp == hexa) hexa += 1 + 4 * hexa_idx++;
  } while (tri != penta || penta != hexa);
  std::cout << tri << std::endl;
}
