#include <iostream>
#include <cmath>

int penta(int n) {
  return n*(3*n-1)/2;
}

int is_penta(int n) {
  int m = static_cast<int>(std::sqrt(2*n/3));
  return n == penta(m+1);
}

int main() {
  std::ios::sync_with_stdio(false);
  auto d_idx = 1, d = penta(d_idx);
  while (true) {
    auto idx = 1;
    while (d > 3*idx+1) {
      auto i = penta(idx++);
      if (is_penta(d+i) && is_penta(d+2*i)) {
        std::cout << d << std::endl;
        return 0;
      }
    }
    d = penta(++d_idx);
  }
}
