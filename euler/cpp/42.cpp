#include <iostream>
#include <string>
#include <cmath>

int main() {
  std::ios::sync_with_stdio(false);
  std::string s;
  auto result = 0;
  while (std::cin.good()) {
    std::getline(std::cin, s, '\"');
    std::getline(std::cin, s, '\"');
    if (!s.empty()) {
      auto val = 0;
      for (auto c : s) val += c-'A'+1;
      auto n = static_cast<int>(std::sqrt(2*val));
      if (n*(n+1) == 2*val) ++result;
    }
  }
  std::cout << result << std::endl;
}
