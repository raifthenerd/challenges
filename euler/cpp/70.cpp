#include <bits/stdc++.h>

using namespace std;

vector<unsigned long long> primes = {2, 3};

bool check_prime(unsigned long long n) {
  if (n == 1) return false;
  for (auto p : primes) {
    if (!(n%p)) return false;
    if (p*p > n) break;
  }
  return true;
}

void gen_prime_until(unsigned long long n) {
  auto m = primes.back() + 2;
  while (m <= n) {
    if (check_prime(m)) primes.push_back(m);
    m += 2;
  }
}

unsigned long long phi(unsigned long long n) {
  unsigned long long result = n;
  for (auto p : primes) {
    if (n % p == 0) {
      result /= p; result *= p-1;
      do {
        n /= p;
      } while (n % p == 0);
    }
    if (p*p > n) {
      result /= n; result *= n-1; break;
    }
  }
  return result;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  const unsigned long long T = 10000000;
  gen_prime_until(T);
  unsigned long long result = 87109;
  double result_ratio = static_cast<double>(result)/phi(result);
  for (unsigned long long n = 2; n < T; ++n) {
    auto phi_n = phi(n);
    auto n_str = to_string(n), phi_n_str = to_string(phi_n);
    if (is_permutation(n_str.begin(), n_str.end(), phi_n_str.begin())) {
      auto ratio = static_cast<double>(n) / phi_n;
      if (result_ratio > ratio) {
        result_ratio = ratio;
        result = n;
      }
    }
  }
  cout << result << endl;
}
