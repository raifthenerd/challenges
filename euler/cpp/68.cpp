#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  vector<int> index = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  unsigned long long result = 0;
  do {
    bool is_solution = true;
    auto total = index[0] + index[1] + index[2], minidx = 1;
    for (auto idx = 3; idx <= 9; idx += 2) {
      if (index[minidx] > index[idx]) minidx = idx;
      if (total != index[idx-1] + index[idx] + index[(idx+1)%10])
        is_solution = false;
    }
    if (!is_solution) continue;
    minidx = (minidx - 1) >> 1;
    unsigned long long tmp = 0;
    for (auto idx = 0; idx < 5; ++idx) {
      auto base = 2 * ((minidx + idx) % 5) + 1;
      tmp *= 1000;
      if (base == 9) tmp *= 10;
      tmp += 100 * index[base] + 10 * index[base-1] + index[(base+1)%10];
    }
    if (result < tmp) result = tmp;
  } while (next_permutation(index.begin(), prev(index.end())));
  cout << result << endl;
}
