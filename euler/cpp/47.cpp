#include <iostream>
#include <list>
#include <algorithm>

std::list<int> primes = {2, 3};

void gen_prime_until(int n) {
  int m = primes.back()+2;
  while (m <= n) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

int num_divisor(int n) {
  if (n == 1) return 1;
  gen_prime_until(n);
  int result = 0;
  for (auto p : primes) {
    if (!(n%p)) ++result;
    if (p > n) break;
  }
  return result;
}

int main() {
  std::ios::sync_with_stdio(false);
  int idx = 644;
  std::list<int> l;
  for (auto i = 0; i < 4; ++i) l.push_back(num_divisor(idx+i));
  while (std::any_of(l.cbegin(), l.cend(), [](int n){ return n < 4; })) {
    l.pop_front();
    l.push_back(num_divisor(4+idx++));
  }
  std::cout << idx << std::endl;
}
