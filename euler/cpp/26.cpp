#include <iostream>

int main() {
  std::ios::sync_with_stdio(false);
  auto result_n = 6, result = 7;
  for (auto i = 1; i < 100; ++i) {
    for (auto j : {1, 3, 7, 9}) {
      auto k = 10*i+j, n = 1, tmp = 10;
      while ((tmp = (10*tmp)%k) != 1) ++n;
      if (n > result_n) {
        result_n = n;
        result = k;
      }
    }
  }
  std::cout << result << std::endl;
}
