#include <iostream>
#include <vector>
#include <algorithm>

std::vector<int> primes = {2, 3};

void gen_prime_until(int n) {
  int m = primes.back()+2;
  while (m <= n) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  gen_prime_until(1000);
  int result_ab = 41, result_n = 40;
  for (auto b : primes) {
    if (b > 1000) break;
    for (auto a = -999; a <= 999; ++a) {
      auto n = 1;
      while (true) {
        auto p = n*n+a*n+b;
        gen_prime_until(p);
        if (!std::binary_search(primes.begin(), primes.end(), p)) break;
        ++n;
      }
      if (n > result_n) {
        result_n = n;
        result_ab = a*b;
      }
    }
  }
  std::cout << result_ab << std::endl;
}
