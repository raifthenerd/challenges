#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int digits = 3;
  unsigned long long n = 5;
  map<string, int> n_permus;
  map<string, unsigned long long> results;
  while (true) {
    if (3 * log10(n) >= digits) {
      for (auto&& x : n_permus) {
        if (x.second == 5) {
          cout << results[x.first] << endl;
          return 0;
        }
      }
      n_permus.clear(); results.clear(); ++digits;
    }
    auto ns = to_string(n*n*n);
    sort(ns.begin(), ns.end());
    if (results.count(ns) == 0) {
      results[ns] = n*n*n;
      n_permus[ns] = 0;
    }
    ++n_permus[ns]; ++n;
  }
}
