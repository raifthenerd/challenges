#include <bits/stdc++.h>

using namespace std;

int gcd(int a, int b) {
  if (a > b) swap(a, b);
  if (a == 0) return b;
  return gcd(b%a, a);
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  set<int> found, unique;
  for (auto x = 2; x <= sqrt(750000); ++x) {
    for (auto y = 1; y < x; ++y) {
      if ((x+y) % 2 == 0 || gcd(x, y) > 1) continue;
      auto k = 1;
      while (k*x*(x+y) <= 750000) {
        auto L = 2*k++*x*(x+y);
        if (found.count(L) == 0) {
          found.insert(L); unique.insert(L);
        } else unique.erase(L);
      }
    }
  }
  cout << unique.size() << endl;
}
