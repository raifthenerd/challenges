#include <bits/stdc++.h>

using namespace std;

unsigned long long gcd(unsigned long long a, unsigned long long b) {
  if (a > b) swap(a, b);
  if (a == 0) return b;
  return gcd(b%a, a);
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long num = 428571, denom = 1000000;  // easily obtained analyitically
  for (auto d = denom; d >= 1; --d) {
    if (d % 7 == 0) continue;
    auto n = (3 * d) / 7;
    if (gcd(n, d) > 1) continue;
    if (n * denom > num *d) {
      num = n; denom = d;
    }
  }
  cout << num << endl;
}
