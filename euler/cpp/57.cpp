#include <iostream>
#include <cmath>

const double log1sq2 = std::log10(1.+std::sqrt(2.)), logsq2 = .5*std::log10(2.);

int main() {
  std::ios::sync_with_stdio(false);
  int result = 0;
  double val = std::log10(985.);
  for (auto i = 7; i < 1000; ++i) {
    if (static_cast<int>(val+logsq2) > static_cast<int>(val)) ++result;
    val += log1sq2;
  }
  std::cout << result << std::endl;
}
