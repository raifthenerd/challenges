#include <bits/stdc++.h>

using namespace std;

list<unsigned int> primes = {2, 3};

bool check_prime(unsigned int n) {
  if (n == 1) return false;
  for (auto p : primes) {
    if (!(n%p)) return false;
    if (p*p > n) break;
  }
  return true;
}
void gen_prime_until(unsigned int n) {
  auto m = primes.back() + 2;
  while (m <= n) {
    if (check_prime(m)) primes.push_back(m);
    m += 2;
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned int p = 9;
  map<unsigned int, set<unsigned int>> ps {{3, {7}}, {7, {3}}};
  while (true) {
    p += 2;
    gen_prime_until(p);
    if (p != primes.back()) continue;
    string s_p = to_string(p);
    for (size_t idx = 1; idx < s_p.size(); ++idx) {
      if (s_p[idx] == '0') continue;
      auto p1 = stoul(s_p.substr(0, idx)),
           p2 = stoul(s_p.substr(idx)),
           p3 = stoul(s_p.substr(idx) + s_p.substr(0, idx));
      if (check_prime(p1) && check_prime(p2) && check_prime(p3)) {
        ps[p1].insert(p2);
        ps[p2].insert(p1);
        vector<unsigned int> tmp;
        set_intersection(ps[p1].begin(), ps[p1].end(),
                         ps[p2].begin(), ps[p2].end(),
                         back_inserter(tmp));
        if (tmp.size() < 3) continue;
        for (auto i = tmp.cbegin(); i != tmp.cend(); ++i) {
          for (auto j = next(i); j != tmp.cend(); ++j) {
            if (ps[*j].count(*i) == 0) continue;
            for (auto k = next(j); k != tmp.cend(); ++k) {
              if (ps[*k].count(*i) > 0 && ps[*k].count(*j) > 0) {
                cout << (*i + *j + *k + p1 + p2) << endl;
                return 0;
              }
            }
          }
        }
      }
    }
  }
}
