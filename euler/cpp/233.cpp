#include <bits/stdc++.h>

using namespace std;

vector<unsigned long long> primes = {2, 3}, primes_1mod4;
unsigned long long result[22] = {};

void add_to_result(unsigned long long n) {
  for (auto idx = 0; idx < 22; ++idx) {
    auto tmp = result[idx] + n;
    result[idx] = tmp%10;
    n = tmp/10;
  }
}

bool check_coprime_1mod4(unsigned long long n) {
  if (n == 1) return true;
  for (auto p : primes_1mod4) {
    if (!(n%p)) return false;
    if (p > n) break;
  }
  return true;
}

bool check_prime(unsigned long long n) {
  if (n == 1) return false;
  for (auto p : primes) {
    if (!(n%p)) return false;
    if (p*p > n) break;
  }
  return true;
}

void gen_prime_until(unsigned long long n) {
  auto m = primes.back() + 2;
  while (m <= n) {
    if (check_prime(m)) {
      primes.push_back(m);
      if (m % 4 == 1) primes_1mod4.push_back(m);
    }
    m += 2;
  }
}

/*
  N = \prod_{p_i : p_i \equiv 1 mod 4}p_i^a_i\times
      \prod_{p_j : p_j \equiv 3 mod 4}p_j^a_j
  ===> f(N) = (-1 + \prod (2a_i+1))/2 = 52
  ===> \prod (2a_i+1) = 105
*/

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  const unsigned long long N = 100000000000;
  gen_prime_until(1 + N / 21125);
  for (auto p1 : primes_1mod4) {
    auto base1 = p1 * p1 * p1; if (base1 > N) break;
    for (auto p2 : primes_1mod4) {
      if (p1 == p2) continue;
      auto base2 = base1 * p2 * p2; if (base2 > N) break;
      for (auto p3 : primes_1mod4) {
        if (p1 == p3 || p2 == p3) continue;
        auto base = base2 * p3; if (base > N) break;
        unsigned long long d = 0;
        while (++d * base <= N)
          if (check_coprime_1mod4(d))
            add_to_result(d * base);
      }
    }
  }
  for (auto p1 : primes_1mod4) {
    auto base1 = p1 * p1 * p1 * p1 * p1 * p1 * p1; if (base1 > N) break;
    for (auto p2 : primes_1mod4) {
      if (p1 == p2) continue;
      auto base = base1 * p2 * p2 * p2; if (base > N) break;
      unsigned long long d = 0;
      while (++d * base <= N)
        if (check_coprime_1mod4(d))
          add_to_result(d * base);
    }
  }
  for (auto p1 : primes_1mod4) {
    auto base1 = p1 * p1 * p1 * p1 * p1 * p1 * p1 * p1 * p1 * p1; if (base1 > N) break;
    for (auto p2 : primes_1mod4) {
      if (p1 == p2) continue;
      auto base = base1 * p2 * p2; if (base > N) break;
      unsigned long long d = 0;
      while (++d * base <= N)
        if (check_coprime_1mod4(d))
          add_to_result(d * base);
    }
  }
  bool print = false;
  for (auto idx = 21; idx >= 0; --idx) {
    if (print || result[idx] != 0) {
      print = true;
      cout << result[idx];
    }
  }
  cout << '\n';
}
