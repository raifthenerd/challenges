#include <iostream>
#include <list>

bool is_palindrome(int n) {
  std::list<int> tmp;
  while (n >= 10) {
    tmp.push_back(n % 10);
    n = n / 10;
  }
  tmp.push_back(n);
  auto it = tmp.cbegin();
  auto rit = tmp.crbegin();
  while (it != tmp.cend() || rit != tmp.crend()) {
    if (*it != *rit) return false;
    ++it; ++rit;
  }
  return true;
}

int main() {
  std::ios::sync_with_stdio(false);
  int result = 0;
  for (auto i = 999; i >= 100; --i) {
    for (auto j = i; j >= 100; --j) {
      int m = i*j;
      if (m > result && is_palindrome(m)) result = m;
    }
  }
  std::cout << result << std::endl;
}
