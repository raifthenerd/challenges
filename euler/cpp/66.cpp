#include <bits/stdc++.h>

using namespace std;

// Pell's equation; algorithm for fundamental solution via continued fractions

class BigInt {
 private:
  static const unsigned long long base = 1000000000;
  static const size_t length = 10;
  unsigned long long digits[length];
 public:
  BigInt(unsigned int n = 0) {
    digits[0] = n;
    for (size_t i = 1; i < length; ++i) digits[i] = 0;
  }
  BigInt& operator=(BigInt other) {
    if (this != &other)
      for (size_t i = 0; i < length; ++i)
        digits[i] = other.digits[i];
    return *this;
  }
  BigInt operator+(const BigInt& other) const {
    BigInt result;
    unsigned long long carry = 0;
    for (size_t i = 0; i < length; ++i) {
      carry += digits[i] + other.digits[i];
      result.digits[i] = carry%base;
      carry /= base;
    }
    return result;
  }
  BigInt operator+(const unsigned int other) const {
    BigInt result;
    unsigned long long carry = other;
    for (size_t i = 0; i < length; ++i) {
      carry += digits[i];
      result.digits[i] = carry%base;
      carry /= base;
    }
    return result;
  }
  BigInt operator*(const BigInt& other) const {
    BigInt result;
    for (size_t i = 0; i < length; ++i) {
      unsigned long long carry = 0;
      for (size_t j = 0; j < length - i; ++j) {
        carry += result.digits[i+j] + digits[j] * other.digits[i];
        result.digits[i+j] = carry%base;
        carry /= base;
      }
    }
    return result;
  }
  BigInt operator*(const unsigned int other) const {
    BigInt result;
    unsigned long long carry = 0;
    for (size_t i = 0; i < length; ++i) {
      carry += digits[i] * other;
      result.digits[i] = carry%base;
      carry /= base;
    }
    return result;
  }
  bool operator>(const BigInt& other) {
    for (size_t i = length; i > 0; --i) {
      if (digits[i-1] > other.digits[i-1]) return true;
      if (digits[i-1] < other.digits[i-1]) return false;
    }
    return false;
  }
  bool operator!=(const BigInt& other) {
    for (size_t i = length; i > 0; --i) {
      if (digits[i-1] != other.digits[i-1]) return true;
    }
    return false;
  }
  string to_str() const {
    string s;
    bool prefix = true;
    for (auto i = length; i > 0; --i) {
      if (prefix) {
        if (digits[i-1] != 0) {
          s = to_string(digits[i-1]);
          prefix = false;
        }
      } else {
        if (digits[i-1] != 0) {
          auto tmp = to_string(digits[i-1]);
          s += string(9 - tmp.size(), '0') + tmp;
        } else {
          s += string(9, '0');
        }
      }
    }
    return s;
  }
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  BigInt sol = 0; unsigned int solD = 0;
  for (unsigned int D = 2; D <= 1000; ++D) {
    double sqrtD = sqrt(D); unsigned int x = static_cast<unsigned int>(sqrtD);
    if (x*x == D) continue;
    auto chain = make_pair(x, 1);
    BigInt a = 1, b = 0, c = 0, d = 1, h = a*x+b, k = c*x+d;
    while (h*h != k*k*D+1) {
      chain.second = (D - chain.first * chain.first) / chain.second;
      x = static_cast<unsigned int>((sqrtD + chain.first) / chain.second);
      chain.first = x * chain.second - chain.first;
      b = a; d = c; a = h; c = k; h = a*x+b; k = c*x+d;
    }
    if (h > sol) {
      sol = h; solD = D;
    }
  }
  cout << solD << endl;
}
