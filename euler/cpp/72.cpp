#include <bits/stdc++.h>

using namespace std;

vector<unsigned long long> primes = {2, 3};

bool check_prime(unsigned long long n) {
  if (n == 1) return false;
  for (auto p : primes) {
    if (!(n%p)) return false;
    if (p*p > n) break;
  }
  return true;
}

void gen_prime_until(unsigned long long n) {
  auto m = primes.back() + 2;
  while (m <= n) {
    if (check_prime(m)) primes.push_back(m);
    m += 2;
  }
}

unsigned long long phi(unsigned long long n) {
  unsigned long long result = n;
  for (auto p : primes) {
    if (n % p == 0) {
      result /= p; result *= p-1;
      do {
        n /= p;
      } while (n % p == 0);
      if (n == 1) break;
    } else if (p*p > n) {
      result /= n; result *= n-1; break;
    }
  }
  return result;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long result = 0;
  gen_prime_until(1000000);
  for (auto d = 2; d <= 1000000; ++d) result += phi(d);
  cout << result << endl;
}
