#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t result[100], aux[100], tmp[100], carry = 0;
  for (auto i = 0; i < 100; ++i) result[i] = aux[i] = tmp[i] = 0;
  result[0] = 3; aux[0] = 2;
  for (size_t n = 3; n <= 100; ++n) {
    for (auto i = 0; i < 100; ++i) {
      carry += (n % 3 == 0 ? 2 * n / 3 : 1) * result[i] + aux[i];
      tmp[i] = carry%10;
      carry /= 10;
    }
    swap(result, aux); swap(result, tmp);
  }
  size_t sum = 0;
  for (auto i = 0; i < 100; ++i) sum += result[i];
  cout << sum << endl;
}
