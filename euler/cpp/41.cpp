#include <iostream>
#include <algorithm>
#include <string>
#include <list>
#include <cmath>

std::list<uint> primes = {2, 3, 5, 7};

bool is_prime(uint m) {
  for (auto p : primes) {
    if (!(m%p)) return false;
    if (p*p > m) return true;
  }
  return true;
}

void gen_prime_until(uint n) {
  uint m = primes.back()+2;
  while (m < n) {
    if (is_prime(m)) primes.push_back(m);
    m += 2;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  gen_prime_until(static_cast<uint>(std::sqrt(987654321)));
  std::string s = "987654321";
  while (true) {
    auto ib = s.begin(), ie = s.end();
    do {
      if (is_prime(std::stoi(s))) {
        std::cout << s << std::endl;
        return 0;
      }
    } while (std::prev_permutation(ib, ie));
    s.erase(0, 1);
  }
}
