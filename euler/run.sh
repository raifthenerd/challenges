#!/usr/bin/env bash

usage() {
    echo "usage: $0 [python|cpp] prob_num"
    exit 1
}

PYTHON='python3'
CXX='g++'
CXXFLAGS='-Wall -lm -O2 -std=gnu++17'

if [ $# != 2 ]; then
    usage
elif [ $1 == "python" ]; then
    $PYTHON -c "import py_compile; py_compile.compile(r'python/$2.py')"
    time $PYTHON python/$2.py
    rm -rf python/__pycache__
elif [ $1 == "cpp" ]; then
    EXEC=`mktemp`; $CXX $CXXFLAGS -o $EXEC cpp/$2.cpp
    time $EXEC
    rm -f $EXEC
else
    usage
fi
