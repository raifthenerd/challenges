;; Write a function which takes a variable number of parameters and returns the maximum value.
;;
;; Restrictions (DON'T use these function(s)): max, max-key

(def __
  (fn [& xs] (last (sort xs)))
  )

(= (__ 1 8 3 4) 8)

(= (__ 30 20) 30)

(= (__ 45 67 11) 67)
