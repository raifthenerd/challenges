;; Write a function which behaves like reduce, but returns each intermediate value of the reduction.  Your function must accept either two or three arguments, and the return sequence must be lazy.
;;
;; Restrictions (DON'T use these function(s)): reductions

(def __
  (fn func
    ([f init coll]
     (lazy-seq (cons init (if (empty? coll) coll
                              (func f (f init (first coll)) (rest coll))))))
    ([f coll]
     (func f (first coll) (rest coll))))
  )

(= (take 5 (__ + (range))) [0 1 3 6 10])

(= (__ conj [1] [2 3 4]) [[1] [1 2] [1 2 3] [1 2 3 4]])

(= (last (__ * 2 [3 4 5])) (reduce * 2 [3 4 5]) 120)
