;; This is the inverse of <a href='92'>Problem 92</a>, but much easier. Given an integer smaller than 4000, return the corresponding roman numeral in uppercase, adhering to the <a href='http://www.numericana.com/answer/roman.htm#valid'>subtractive principle</a>.

(def __
  (partial
   (fn [s n]
     (cond
       (>= n 1000) (recur (conj s \M) (- n 1000))
       (>= n 900) (recur (conj s \C \M) (- n 900))
       (>= n 500) (recur (conj s \D) (- n 500))
       (>= n 400) (recur (conj s \C \D) (- n 400))
       (>= n 100) (recur (conj s \C) (- n 100))
       (>= n 90) (recur (conj s \X \C) (- n 90))
       (>= n 50) (recur (conj s \L) (- n 50))
       (>= n 40) (recur (conj s \X \L) (- n 40))
       (>= n 10) (recur (conj s \X) (- n 10))
       (>= n 9) (recur (conj s \I \X) (- n 9))
       (>= n 5) (recur (conj s \V) (- n 5))
       (>= n 4) (recur (conj s \I \V) (- n 4))
       (>= n 1) (recur (conj s \I) (- n 1))
       :else (apply str s)))
   [])
  )

(= "I" (__ 1))

(= "XXX" (__ 30))

(= "IV" (__ 4))

(= "CXL" (__ 140))

(= "DCCCXXVII" (__ 827))

(= "MMMCMXCIX" (__ 3999))

(= "XLVIII" (__ 48))
