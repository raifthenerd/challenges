;; Write a function which returns a sequence of lists of x items each.  Lists of less than x items should not be returned.
;;
;; Restrictions (DON'T use these function(s)): partition, partition-all

(def __
  (fn foo [n xs]
    (if (<= n (count xs))
      (cons (take n xs) (foo n (drop n xs)))))
  )

(= (__ 3 (range 9)) '((0 1 2) (3 4 5) (6 7 8)))

(= (__ 2 (range 8)) '((0 1) (2 3) (4 5) (6 7)))

(= (__ 3 (range 8)) '((0 1 2) (3 4 5)))
