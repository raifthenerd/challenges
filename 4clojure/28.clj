;; Write a function which flattens a sequence.
;;
;; Restrictions (DON'T use these function(s)): flatten

(def __
  (fn myflatten [[x & xs]]
    (if x
      (if (coll? x)
        (concat (myflatten x) (myflatten xs))
        (cons x (myflatten xs)))))
  )

(= (__ '((1 2) 3 [4 [5 6]])) '(1 2 3 4 5 6))

(= (__ ["a" ["b"] "c"]) '("a" "b" "c"))

(= (__ '((((:a))))) '(:a))
