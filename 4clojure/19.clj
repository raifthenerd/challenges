;; Write a function which returns the last element in a sequence.
;;
;; Restrictions (DON'T use these function(s)): last

(def __
  #(first (reverse %))
  )

(= (__ [1 2 3 4 5]) 5)

(= (__ '(5 4 3)) 3)

(= (__ ["b" "c" "d"]) "d")
