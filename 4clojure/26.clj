;; Write a function which returns the first X fibonacci numbers.

(def __
  #(take % (map first (iterate (fn [[a b]] [b (+ a b)]) [1 1])))
  )

(= (__ 3) '(1 1 2))

(= (__ 6) '(1 1 2 3 5 8))

(= (__ 8) '(1 1 2 3 5 8 13 21))
