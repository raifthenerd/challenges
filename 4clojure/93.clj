;; Write a function which flattens any nested combination of sequential things (lists, vectors, etc.), but maintains the lowest level sequential items.  The result should be a sequence of sequences with only one level of nesting.

(def __
  (letfn [(nest [level]
            (fn [f]
              (fn [x]
                (map (level f) x))))]
    (partial
     (fn [f xs]
       (let [default (if (vector? xs) []
                         (if (seq? xs) '()))
             test (->> xs
                       (((nest f) coll?))
                       flatten)]
         (if (every? false? test) xs
             (if (some false? test)
               (recur f (reduce
                         (fn [m k]
                           (if (->> k
                                    ((f coll?))
                                    flatten
                                    (some false?))
                             (conj m k)
                             (apply conj m k)))
                         default xs))
               (recur (nest f) xs)))))
     identity))
  )

(= (__ [["Do"] ["Nothing"]])
   [["Do"] ["Nothing"]])

(= (__ [[[[:a :b]]] [[:c :d]] [:e :f]])
   [[:a :b] [:c :d] [:e :f]])

(= (__ '((1 2)((3 4)((((5 6)))))))
   '((1 2)(3 4)(5 6)))
