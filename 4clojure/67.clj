;; Write a function which returns the first x
;;
;; number of prime numbers.

(def __
  (fn [n]
    (take n
          (filter
           (fn [x]
             (every? #(pos? (mod x %)) (range 2 (inc (int (Math/sqrt x))))))
           (iterate inc 2))))
  )

(= (__ 2) [2 3])

(= (__ 5) [2 3 5 7 11])

(= (last (__ 100)) 541)
