;; Write a function which takes a collection of integers as an argument.  Return the count of how many elements are smaller than the sum of their squared component digits.  For example: 10 is larger than 1 squared plus 0 squared; whereas 15 is smaller than 1 squared plus 5 squared.

(def __
  (fn [xs]
    (count (filter
            (fn [x] (< x (->> x str
                              (map #(- (int %) (int \0)))
                              (map #(* % %))
                              (apply +))))
            xs)))
  )

(= 8 (__ (range 10)))

(= 19 (__ (range 30)))

(= 50 (__ (range 100)))

(= 50 (__ (range 1000)))
