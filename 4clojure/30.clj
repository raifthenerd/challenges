;; Write a function which removes consecutive duplicates from a sequence.

(def __
  (fn mycomp [[x & xs]]
    (if x
      (if (= x (first xs))
        (mycomp xs)
        (cons x (mycomp xs)))))
  )

(= (apply str (__ "Leeeeeerrroyyy")) "Leroy")

(= (__ [1 1 2 3 3 2 2 3]) '(1 2 3 2 3))

(= (__ [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2]))
