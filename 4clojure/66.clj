;; Given two integers, write a function which
;;
;; returns the greatest common divisor.

(def __
  (fn [a b]
    (if (> a b)
      (recur b a)
      (if (= a 0)
        b
        (recur a (rem b a)))))
  )

(= (__ 2 4) 2)

(= (__ 10 5) 5)

(= (__ 5 7) 1)

(= (__ 1023 858) 33)
