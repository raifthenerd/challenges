;; Write a function which drops every Nth item from a sequence.

(def __
  (fn foo [xs n]
    (if (not (empty? xs))
      (concat (take (dec n) xs) (foo (drop n xs) n))))
  )

(= (__ [1 2 3 4 5 6 7 8] 3) [1 2 4 5 7 8])

(= (__ [:a :b :c :d :e :f] 2) [:a :c :e])

(= (__ [1 2 3 4 5 6] 4) [1 2 3 5 6])
