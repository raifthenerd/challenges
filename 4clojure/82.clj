;; A word chain consists of a set of words ordered so that each word differs by only one letter from the words directly before and after it.  The one letter difference can be either an insertion, a deletion, or a substitution.  Here is an example word chain:<br/><br/>cat -> cot -> coat -> oat -> hat -> hot -> hog -> dog<br/><br/>Write a function which takes a sequence of words, and returns true if they can be arranged into one continous word chain, and false if they cannot.

(def __
  (fn [xs]
    (let [drop-nth (fn [coll index]
                     (concat (take index coll) (drop (inc index) coll)))
          is-edge? (fn [x y]
                     (case (- (count x) (count y))
                       0 (= (->> (map = x y) (filter false?) count) 1)
                       1 (some (partial = y)
                               (map #(apply str (drop-nth x %))
                                    (-> x count range)))
                       -1 (recur y x)
                       false))
          is-path? (fn [xs]
                     (every? true? (map is-edge? (rest xs) (drop-last xs))))
          permutation (fn f [xs]
                        (if (empty? xs)
                          ['()]
                          (apply concat
                                 (map #(map (partial cons %)
                                            (f (disj xs %)))
                                      xs))))]
      (true? (some identity (map is-path? (permutation xs))))))
  )

(= true (__ #{"hat" "coat" "dog" "cat" "oat" "cot" "hot" "hog"}))

(= false (__ #{"cot" "hot" "bat" "fat"}))

(= false (__ #{"to" "top" "stop" "tops" "toss"}))

(= true (__ #{"spout" "do" "pot" "pout" "spot" "dot"}))

(= true (__ #{"share" "hares" "shares" "hare" "are"}))

(= false (__ #{"share" "hares" "hare" "are"}))
