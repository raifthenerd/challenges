;; Write a function which takes a string and returns a new string containing only the capital letters.

(def __
  #(apply str (re-seq #"[A-Z]+" %))
  )

(= (__ "HeLlO, WoRlD!") "HLOWRD")

(empty? (__ "nothing"))

(= (__ "$#A(*&987Zf") "AZ")
